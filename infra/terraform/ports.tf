locals {
  ports = {
    ssh_tcp          = ["22"],
    http_tcp         = ["80"],
    https_tcp        = ["443"],
    service_tcp      = ["8080"],
    consul_tcp       = ["8600", "8301", "8302", "8500", "8501", "8502", "8300"],
    consul_udp       = ["8600", "8301", "8302"],
    node_exp_tcp     = ["9100"],
    graylog_tcp      = ["9000", "9200", "9300", "27017", "9833", "12201"],
    cadvisor_tcp     = ["9280"],
    prom_tcp         = ["9090"],
    grafana_tcp      = ["3000"],
    victoria_tcp     = ["8428"],
    alertmanager_tcp = ["9093", "9094"]
    sonar_tcp        = ["9000"],
    psql_exp_tcp     = ["9187"],
    cloudw_exp_tcp   = ["5000"],
    eph_tcp          = ["1024", "65535"]
  }
}

locals {
  server_ports = {
    alb_server_tcp     = concat(local.ports.http_tcp, local.ports.https_tcp)
    alb_server_udp     = concat([], [])
    asg_server_tcp     = concat(local.ports.service_tcp, local.ports.node_exp_tcp, local.ports.cadvisor_tcp, local.ports.consul_tcp)
    asg_server_udp     = concat(local.ports.consul_udp, [])
    web_server_tcp     = concat(local.ports.node_exp_tcp, local.ports.consul_tcp)
    web_server_udp     = concat(local.ports.consul_udp, [])
    graylog_server_tcp = concat(local.ports.graylog_tcp, local.ports.node_exp_tcp, local.ports.consul_tcp)
    graylog_server_udp = concat(local.ports.consul_udp, [])
    cn_server_tcp      = concat(local.ports.node_exp_tcp, local.ports.cadvisor_tcp, local.ports.consul_tcp, local.ports.cloudw_exp_tcp)
    cn_server_udp      = concat(local.ports.consul_udp, [])
    mon_server_tcp     = concat(local.ports.node_exp_tcp, local.ports.prom_tcp, local.ports.grafana_tcp, local.ports.alertmanager_tcp, local.ports.victoria_tcp, local.ports.consul_tcp)
    mon_server_udp     = concat(local.ports.consul_udp, [])
    sonar_server_tcp   = concat(local.ports.node_exp_tcp, local.ports.cadvisor_tcp, local.ports.consul_tcp, local.ports.sonar_tcp, local.ports.psql_exp_tcp)
    sonar_server_udp   = concat(local.ports.consul_udp, [])
    consul_server_tcp  = concat(local.ports.consul_tcp, [])
    consul_server_udp  = concat(local.ports.consul_udp, [])
    os_domain_tcp      = concat(local.ports.https_tcp, [])
  }
}

locals {
  subnet_ports = {
    alb_public_tcp    = distinct(concat(local.server_ports.alb_server_tcp, []))
    alb_public_udp    = distinct(concat(local.server_ports.alb_server_udp, []))
    web_public_tcp    = distinct(concat(local.server_ports.web_server_tcp, []))
    web_public_udp    = distinct(concat(local.server_ports.web_server_udp, []))
    asg_private_tcp   = distinct(concat(local.server_ports.asg_server_tcp, local.server_ports.consul_server_tcp))
    asg_private_udp   = distinct(concat(local.server_ports.asg_server_udp, local.server_ports.consul_server_udp))
    infra_private_tcp = distinct(concat(local.server_ports.graylog_server_tcp, local.server_ports.cn_server_tcp, local.server_ports.mon_server_tcp, local.server_ports.sonar_server_tcp, local.server_ports.consul_server_tcp))
    infra_private_udp = distinct(concat(local.server_ports.graylog_server_udp, local.server_ports.cn_server_udp, local.server_ports.mon_server_udp, local.server_ports.sonar_server_udp, local.server_ports.consul_server_udp))
  }
}
