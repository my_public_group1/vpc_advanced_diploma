resource "null_resource" "server_certificate" {
  provisioner "local-exec" {
    environment = merge(local.provisioner_base_env, {
    })
    command = "${path.module}/scripts/prepare_easyrsa.sh"
  }
}

resource "null_resource" "client_certificate" {
  count      = length(local.clients)
  depends_on = [aws_acm_certificate.server_cert]

  provisioner "local-exec" {
    environment = merge(local.provisioner_base_env, {
      "CLIENT_CERT_NAME" = local.clients[count.index]
    })

    command = "${path.module}/scripts/create_client.sh"
  }
}

resource "null_resource" "export_clients_vpn_config" {
  depends_on = [null_resource.client_certificate]
  count      = length(local.clients)
  triggers = {
    client = local.clients[count.index]
  }

  provisioner "local-exec" {
    environment = merge(local.provisioner_base_env, {
      "CLIENT_VPN_ID"    = aws_ec2_client_vpn_endpoint.client_vpn.id,
      "CLIENT_CERT_NAME" = local.clients[count.index],
      "TENANT_NAME"      = var.aws_tenant_name,
      "ENV_NAME"         = var.env
    })
    command = "${path.module}/scripts/export_client_vpn_config.sh"
  }
}

resource "null_resource" "connect_to_vpn" {
  depends_on = [aws_ec2_client_vpn_network_association.client_vpn]

  provisioner "local-exec" {
    command = "sleep 1m && service openvpn-client@${local.clients[0]}.${var.aws_tenant_name}.${var.env} restart"
  }
}
