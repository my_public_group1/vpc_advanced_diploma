locals {
  provisioner_base_env = {
    "CERT_ISSUER"     = var.cert_issuer
    "KEY_SAVE_FOLDER" = var.key_save_folder
    "SOPS_KMS_ARN"    = aws_kms_key.sops.arn
    "REGION"          = var.region
    "ENV"             = var.env
    "PKI_FOLDER_NAME" = "pki_${var.env}"
    "MODULE_PATH"     = path.module
    "CONCURRENCY"     = "true"
    "AWSCLIPROFILE"   = var.aws_cli_profile_name
  }

  clients = concat(var.clients)
}
