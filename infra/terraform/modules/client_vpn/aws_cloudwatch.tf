resource "aws_cloudwatch_log_group" "client_vpn" {
  name = "${var.cloudwatch_log_group}_${terraform.workspace}"
}

resource "aws_cloudwatch_log_stream" "client_vpn" {
  name           = "${var.cloudwatch_log_stream}_${terraform.workspace}"
  log_group_name = aws_cloudwatch_log_group.client_vpn.name
}
