resource "aws_ec2_client_vpn_endpoint" "client_vpn" {
  depends_on             = [aws_acm_certificate.server_cert]
  description            = var.vpn_name
  server_certificate_arn = aws_acm_certificate.server_cert.arn
  client_cidr_block      = var.client_cidr_block
  split_tunnel           = true
  dns_servers            = var.dns_servers
  vpc_id                 = var.vpc_id
  security_group_ids     = [aws_security_group.vpn_endpnt_sg.id]

  lifecycle {
    ignore_changes = [server_certificate_arn, authentication_options]
  }

  authentication_options {
    type                        = var.client_auth
    active_directory_id         = var.active_directory_id
    root_certificate_chain_arn  = aws_acm_certificate.server_cert.arn
    saml_provider_arn           = var.saml_provider_arn
  }

  connection_log_options {
    enabled               = var.cloudwatch_enabled
    cloudwatch_log_group  = aws_cloudwatch_log_group.client_vpn.name
    cloudwatch_log_stream = aws_cloudwatch_log_stream.client_vpn.name
  }

  provisioner "local-exec" {
    environment = merge(local.provisioner_base_env,
      { "CLIENT_VPN_ID" = self.id },
      { "TARGET_IDS" = join(" ", data.aws_subnets.subnets.ids) })
    command = "${path.module}/scripts/authorize_client.sh"
  }

  tags = {
    Name = var.vpn_name
  }
}
