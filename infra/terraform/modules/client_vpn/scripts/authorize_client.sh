#!/usr/bin/env bash
set -x

for id in $TARGET_IDS
do
    current_cidr=$(aws ec2 describe-subnets --subnet-ids $id --query "Subnets[*].CidrBlock" --output text)
    aws ec2 authorize-client-vpn-ingress --profile $AWSCLIPROFILE --client-vpn-endpoint-id $CLIENT_VPN_ID --target-network-cidr $current_cidr --authorize-all-groups
done
