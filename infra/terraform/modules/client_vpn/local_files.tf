data "local_file" "server_private_key" {
  depends_on = [null_resource.server_certificate]
  filename = null_resource.server_certificate.id > 0 ? "pki_${var.env}/${var.key_save_folder}/server.key" : ""
}

data "local_file" "server_certificate_body" {
  depends_on = [null_resource.server_certificate]
  filename = null_resource.server_certificate.id > 0 ? "pki_${var.env}/${var.key_save_folder}/server.crt" : ""
}

data "local_file" "server_certificate_chain" {
  depends_on = [null_resource.server_certificate]
  filename = null_resource.server_certificate.id > 0 ? "pki_${var.env}/${var.key_save_folder}/ca.crt" : ""
}
