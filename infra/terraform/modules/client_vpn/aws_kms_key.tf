resource "aws_kms_key" "sops" {
  description = "A KMS key used by SOPS to safely store easy-rsa secrets in Git."

  tags = {
    "Terraform" = "true"
  }
}
