resource "aws_ec2_client_vpn_network_association" "client_vpn" {
  count = length(var.private_subnet_ids)
  client_vpn_endpoint_id = aws_ec2_client_vpn_endpoint.client_vpn.id
  subnet_id              = var.private_subnet_ids[count.index]
}
