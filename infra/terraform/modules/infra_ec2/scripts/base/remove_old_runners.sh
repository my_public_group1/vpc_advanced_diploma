#!/usr/bin/env bash
set -o nounset
set -o errexit

# Required tools:
# curl
# jq
# tr

gitlabUrl="https://gitlab.com"  # todo: point to gitlab url to cleanup
gitlabApiToken=$1  # todo: fill with admin api token

# Offline runners
offlineRunnerIds=$(curl --header "PRIVATE-TOKEN: ${gitlabApiToken}" "${gitlabUrl}/api/v4/runners?status=offline" | jq '.[].id' | tr '\n' ' ')

# Remove runners
for id in ${offlineRunnerIds}; do
    echo "deleting [${id}]"
    curl --request DELETE --header "PRIVATE-TOKEN: ${gitlabApiToken}" "${gitlabUrl}/api/v4/runners/${id}"
done
