#!/usr/bin/env bash

ansible-playbook -u $3 -i $1/$2 --private-key $4.pem $1/$7/main.yml -e "def_path=$5 tf_workspace=$6" -b -v
