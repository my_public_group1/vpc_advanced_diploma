#!/usr/bin/env bash

rm -f $1/$2
cat >$1/$2 <<EOL
[consul_instances]
$3
EOL
