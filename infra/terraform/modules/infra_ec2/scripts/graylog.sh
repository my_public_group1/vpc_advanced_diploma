#!/usr/bin/env bash
set -o nounset
set -o errexit

play_path="graylog_server"
workspace=$TF_WORKSPACE
new_ws="_$workspace"
inv_file="$play_path$workspace"
module_path=$MODULE_PATH
vpc_start=$VPC_STARTSWITH

#create control node inv file
$module_path/scripts/base/create_inv.sh $ANS_PATH $inv_file.inv $SELF_PRIVATE_IP

cat >$DEFS_DIR/fluent_bit.yml <<EOL
graylog_endpoint: $SELF_PRIVATE_IP
EOL
cat >$DEFS_DIR/consul_client.yml <<EOL
consul_join: $CONSUL_JOIN_IPS
vpc_startswith: "$vpc_start"
EOL
cat >$DEFS_DIR/enclave.yml <<EOL
current_domain: $ROUTE53_HOSTED_ZONE_NAME
env_domain: $ENV_DOMAIN
aws_opensearch_endpoint: $OS_ENDPOINT
certificate_arn: $CERT_ARN
EOL
cat >$DEFS_DIR/cn.yml <<EOL
runner_tags: aws,docker,$TF_WORKSPACE
gitlab_token: $GITLAB_TOKEN
EOL
cat >$DEFS_DIR/os.yml <<EOL
os_endpoint: $OS_ENDPOINT
os_user: $OS_USER
os_pass: $OS_PASS
EOL
cat >$DEFS_DIR/monitoring.yml <<EOL
grafana_user: $GRAFANA_USER
grafana_password: $GRAFANA_PASS
pagerduty_user_url: $PD_URL
pagerduty_user_key: $PD_KEY
EOL
cat >$DEFS_DIR/graylog.yml <<EOL
graylog_password_secret: $GL_SECRET
graylog_password_unencrypted: $GL_PASS
EOL
cat >$DEFS_DIR/sonarqube.yml <<EOL
sonar_password: $SONAR_PASS
psql_username: $PSQL_USER
psql_password: $PSQL_PASS
EOL
