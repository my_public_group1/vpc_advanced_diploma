#!/usr/bin/env bash

set -o nounset
set -o errexit

play_path="control_node_server"
workspace=$TF_WORKSPACE
new_ws="_$workspace"
inv_file="$play_path$workspace"
module_path=$MODULE_PATH

#remove old runners
$module_path/scripts/base/remove_old_runners.sh $GITLAB_API_TOKEN

#create control node inv file
$module_path/scripts/base/create_inv.sh $ANS_PATH $inv_file.inv $SELF_PRIVATE_IP

#run ansible-playbook
$module_path/scripts/base/run_ansible_playbook.sh $ANS_PATH $inv_file.inv $HOST_USER $KEY_NAME $DEF_PATH $TF_WORKSPACE $play_path

#remove cn inv file
$module_path/scripts/base/remove_inv.sh $ANS_PATH $inv_file.inv
