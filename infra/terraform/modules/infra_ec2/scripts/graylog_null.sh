#!/usr/bin/env bash

set -o nounset
set -o errexit

play_path="graylog_server"
workspace=$TF_WORKSPACE
new_ws="_$workspace"
inv_file="$play_path$workspace"
module_path=$MODULE_PATH

#run ansible-playbook
$module_path/scripts/base/run_ansible_playbook.sh $ANS_PATH $inv_file.inv $HOST_USER $KEY_NAME $DEF_PATH $TF_WORKSPACE $play_path

#remove inv file
$module_path/scripts/base/remove_inv.sh $ANS_PATH $inv_file.inv
