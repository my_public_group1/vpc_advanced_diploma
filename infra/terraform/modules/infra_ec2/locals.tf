locals {
  key_path = "${path.root}/${var.key_name}.pem"
  json_consul_ips = jsonencode(var.consul_join_ips)
  vpc_startswith = regex("^\\d+", var.vpc_cidr_block)
}
