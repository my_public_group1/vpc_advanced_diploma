resource "aws_security_group" "lb" {
  name = "Dynamic Security Group for ALB (${terraform.workspace})"
  vpc_id = var.vpc_id

  dynamic "ingress" {
    # Зададим правило, по каким портам можно обращаться к нашим серверам
    for_each = var.server_ports.alb_server_tcp
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = var.allowed_cidr_blocks
    }
  }

#  dynamic "ingress" {
#    # Зададим правило, по каким портам можно обращаться к нашим серверам
#    for_each = var.server_ports.alb_server_udp
#    content {
#      from_port   = ingress.value
#      to_port     = ingress.value
#      protocol    = "udp"
#      cidr_blocks = var.allowed_cidr_blocks
#    }
#  }

  dynamic "egress" {
    for_each = var.server_ports.alb_server_tcp
    content {
      from_port   = egress.value
      to_port     = egress.value
      protocol    = "tcp"
      cidr_blocks = var.allowed_cidr_blocks
    }
  }

  egress {
    from_port   = var.ports.eph_tcp[0]
    to_port     = var.ports.eph_tcp[1]
    protocol    = "tcp"
    cidr_blocks = var.allowed_cidr_blocks
  }

  tags = {
    Name  = "Web access for ALB (${terraform.workspace})"
  }
}

