resource "aws_lb_listener" "public_http" {
  load_balancer_arn = aws_lb.public_lb.arn
  port              = var.ports.http_tcp[0]
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = var.ports.https_tcp[0]
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_lb_listener" "public_https" {
  load_balancer_arn = aws_lb.public_lb.arn
  port              = var.ports.https_tcp[0]
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = var.certificate_arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.lb_asg.arn
  }
}
