resource "aws_lb" "public_lb" {
  name               = "skill-lb-${terraform.workspace}"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.lb.id]
  subnets            = [var.public_subnet_ids[1], var.public_subnet_ids[2]]

  enable_deletion_protection = false

  access_logs {
    bucket  = var.bucket_name
    prefix  = var.bucket_key_path_lb_logs
    enabled = "true"
  }

  tags = {
    Name = "Skill-ALB-${terraform.workspace}"
    Environment = "${terraform.workspace}"
  }
}
