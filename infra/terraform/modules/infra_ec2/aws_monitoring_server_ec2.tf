# Запускаем Prometheus сервер
resource "aws_instance" "mon_server" {
  # с выбранным образом 
  ami                    = var.aws_ami_ubuntu_id
  # и размером (количество ЦПУ и памяти зависит от этой директивы) 
  instance_type          = "t2.small"
  vpc_security_group_ids = [aws_security_group.mon_server.id]
  key_name               = var.key_name
  subnet_id              = var.private_subnet_ids[0]
  tags = {
    AMI =  "${var.aws_ami_ubuntu_id}"
    Name  = "Monitoring Server (${terraform.workspace})"
  }

  depends_on = [aws_instance.graylog_server]

  provisioner "remote-exec" {
    inline = ["sleep 10s",
              "sudo apt update -y",
              "sudo apt-get install python3 -y"]

    connection {
      host        = "${self.private_ip}"
      type        = "ssh"
      user        = var.ubuntu_user
      private_key = "${file(local.key_path)}"
    }
  }

  provisioner "local-exec" {
    environment = {
      "ANS_PATH"                 = var.ans_path,
      "KEY_NAME"                 = var.key_name,
      "DEF_PATH"                 = var.def_path,
      "TF_WORKSPACE"             = terraform.workspace,
      "HOST_USER"                = var.ubuntu_user,
      "SELF_PRIVATE_IP"          = self.private_ip,
      "MODULE_PATH"              = path.module
    }
    command = "${path.module}/scripts/monitoring.sh"
  }
}
