resource "aws_lb_target_group" "lb_asg" {
  name     = "Skill-PubALB-TG-${terraform.workspace}"
  port     = var.ports.service_tcp[0]
  protocol = "HTTP"
  vpc_id   = var.vpc_id
}
