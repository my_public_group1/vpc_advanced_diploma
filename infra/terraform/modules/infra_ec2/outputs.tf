output "web_public_ip" {
  value = aws_instance.web.public_ip
}

output "elb_dns_name" {
  value = aws_lb.public_lb.dns_name
}

output "elb_zone_id" {
  value = aws_lb.public_lb.zone_id
}
