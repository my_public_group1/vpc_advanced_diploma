# Создаём Launch Configuration - это сущность, которая определяет конфигурацию запускаемых серверов. Размер, , 
resource "aws_launch_configuration" "asg" {
  name_prefix     = "DOCKER-${terraform.workspace}-"
  # какой будет использоваться образ
  image_id        = var.aws_ami_ubuntu_id
  # Размер машины (CPU и память)
  instance_type   = "t2.micro"
  # какие права доступа
  security_groups = [aws_security_group.asg.id]
  # какие следует запустить скрипты при создании сервера
  user_data       = templatefile("${path.module}/scripts/asg_playbook.tftpl",
                      { consul_join_ips = "${local.json_consul_ips}",
                        graylog_endpoint = "${aws_instance.graylog_server.private_ip}",
                        gitlab_repo = "${var.gitlab_repo}",
                        bucket_key_path = "${var.bucket_key_path_version}",
                        tf_workspace = "${terraform.workspace}",
                        vpc_startswith = "${local.vpc_startswith}" 
                      })
  # какой SSH ключ будет использоваться 
  key_name = "${var.key_name}"
  # Если мы решим обновить инстанс, то, прежде, чем удалится старый инстанс, который больше не нужен, должен запуститься новый
  lifecycle {
    create_before_destroy = true
  }

  depends_on = [aws_instance.graylog_server]

}


# AWS Autoscaling Group для указания, сколько нам понадобится инстансов 
resource "aws_autoscaling_group" "web" {
  name                      = "ASG-${aws_launch_configuration.asg.name}-${terraform.workspace}"
  launch_configuration      = aws_launch_configuration.asg.name
  min_size                  = 2
  max_size                  = 4
  desired_capacity          = 2
  health_check_type         = "EC2"
  wait_for_capacity_timeout = "30m"
  service_linked_role_arn   = var.iam_service_linked_role_asg_arn
  # и в каких подсетях, каких Дата центрах их следует разместить
  vpc_zone_identifier       = [var.private_subnet_ids[1], var.private_subnet_ids[2]]
  # Ссылка на балансировщик нагрузки, который следует использовать 
  target_group_arns         = [aws_lb_target_group.lb_asg.arn]

  dynamic "tag" {
    for_each = {
      Name = "Docker-ASG-${terraform.workspace}"
    }
    content {
      key                 = tag.key
      value               = tag.value
      propagate_at_launch = true
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}

