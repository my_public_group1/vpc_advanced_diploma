# Запускаем GrayLog сервер
resource "aws_instance" "graylog_server" {
  # с выбранным образом
  ami                    = var.aws_ami_ubuntu_id
  # и размером (количество ЦПУ и памяти зависит от этой директивы)
  instance_type          = "t2.2xlarge"
  vpc_security_group_ids = [aws_security_group.graylog_server.id]
  key_name = var.key_name
  subnet_id = var.private_subnet_ids[0]

  root_block_device {
    volume_size = 30
  }

  tags = {
    AMI =  "${var.aws_ami_ubuntu_id}"
    Name  = "GrayLog Server (${terraform.workspace})"
  }

  provisioner "remote-exec" {
    inline = ["sleep 10s",
              "sudo apt-get update -y",
              "sleep 10s",
              "sudo apt-get install python3 -y"]

    connection {
      host        = "${self.private_ip}"
      type        = "ssh"
      user        = var.ubuntu_user
      private_key = "${file(local.key_path)}"
    }
  }

  provisioner "local-exec" {
    environment = {
      "ANS_PATH"                 = var.ans_path,
      "SELF_PRIVATE_IP"          = self.private_ip,
      "DEFS_DIR"                 = var.defs_dir,
      "TF_WORKSPACE"             = terraform.workspace,
      "ROUTE53_HOSTED_ZONE_NAME" = var.route53_hosted_zone_name,
      "ENV_DOMAIN"               = var.env_domain,
      "GITLAB_TOKEN"             = var.gitlab_token,
      "OS_ENDPOINT"              = var.aws_opensearch_endpoint,
      "OS_USER"                  = var.os_user,
      "OS_PASS"                  = var.os_pass,
      "GRAFANA_USER"             = var.grafana_user,
      "GRAFANA_PASS"             = var.grafana_pass,
      "PD_URL"                   = var.pagerduty_user_url,
      "PD_KEY"                   = var.pagerduty_user_key,
      "GL_SECRET"                = var.graylog_secret,
      "GL_PASS"                  = var.graylog_password,
      "SONAR_PASS"               = var.sonar_password,
      "PSQL_USER"                = var.psql_username,
      "PSQL_PASS"                = var.psql_password,
      "CERT_ARN"                 = var.certificate_arn,
      "CONSUL_JOIN_IPS"          = local.json_consul_ips,
      "MODULE_PATH"              = path.module,
      "VPC_STARTSWITH"           = local.vpc_startswith
    }
    command = "${path.module}/scripts/graylog.sh"
  }
}
