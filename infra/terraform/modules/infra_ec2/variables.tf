variable "route53_hosted_zone_name" {
  type = string
}

variable "aws_access_key_id" {
  type = string
}

variable "aws_secret_access_key" {
  type = string
}

variable "gitlab_repo" {
  type = string
}

variable "gitlab_token" {
  type = string
}

variable "gitlab_access_api_token" {
  type = string
}

variable "os_user" {
  type = string
}

variable "os_pass" {
  type = string
}

variable "grafana_user" {
  type = string
}

variable "grafana_pass" {
  type = string
}

variable "pagerduty_user_url" {
  type = string
}

variable "pagerduty_user_key" {
  type = string
}

variable "graylog_secret" {
  type = string
}

variable "graylog_password" {
  type = string
}

variable "sonar_password" {
  type = string
}

variable "psql_username" {
  type = string
  default = "sonar"
}

variable "psql_password" {
  type = string
}

variable "env_domain" {
  type = string
}

variable "defs_dir" {
  type = string
}

variable "client_cidr_block" {
  description = "VPN CIDR block, must not overlap with VPC CIDR. Client cidr block must be at least a /22 range."
  type        = string
}

variable "ports" {
  type = map(list(string))
}

variable "server_ports" {
  type = map(list(string))
}

variable "region" {
  type = string
}

variable "bucket_name" {
  type = string
}

variable "bucket_key_path_version" {
  type = string
}

variable "bucket_key_path_lb_logs" {
  type = string
}

variable "aws_metaname" {
  type = string
}

variable "allowed_cidr_blocks" {
  type = list
}

variable "ans_path" {
  type = string
}

variable "def_path" {
  type = string
}

variable "ubuntu_user" {
  type = string
}

variable "amazon_linux_user" {
  type = string
}

variable "aws_ami_acm_enclave_id" {
  type = string
}

variable "aws_ami_ubuntu_id" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "private_subnet_ids" {
  type = list(string)
}

variable "public_subnet_ids" {
  type = list(string)
}

variable "key_name" {
  type = string
}

variable "vpc_cidr_block" {
  type = string
}

variable "certificate_arn" {
  type = string
}

variable "client_vpn_sg_id" {
  type = string
}

variable "aws_opensearch_endpoint" {
  type = string
}

variable "iam_service_linked_role_asg_arn" {
  type = string
}

variable "iam_instance_profile_cn_ec2_name" {
  type = string
}

variable "iam_instance_profile_enclave_acm_name" {
  type = string
}

variable "consul_join_ips" {
  type = list(string)
}

