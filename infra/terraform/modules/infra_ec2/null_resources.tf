resource "null_resource" "run_ansible_graylog_server" {
  provisioner "local-exec" {
    environment = {
      "ANS_PATH"                 = var.ans_path,
      "KEY_NAME"                 = var.key_name,
      "DEF_PATH"                 = var.def_path,
      "TF_WORKSPACE"             = terraform.workspace,
      "HOST_USER"                = var.ubuntu_user,
      "MODULE_PATH"              = path.module
    }
    command = "${path.module}/scripts/graylog_null.sh"
  }
  depends_on = [aws_instance.graylog_server]
}
