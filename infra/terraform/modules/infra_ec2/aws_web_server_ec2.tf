resource "aws_instance" "web" {
  ami = var.aws_ami_acm_enclave_id
  instance_type = "m5.2xlarge"
  key_name = var.key_name
  vpc_security_group_ids = ["${aws_security_group.tfwebSG.id}"]
  subnet_id = var.public_subnet_ids[0]
  iam_instance_profile = var.iam_instance_profile_enclave_acm_name

  enclave_options {
    enabled = true
  }

  tags = {
    Name = "Nitro Enclave NGINX ${terraform.workspace}"
  }

  provisioner "remote-exec" {
    inline = ["sleep 10s",
              "sudo yum check-update -y",
              "sleep 10s",
              "sudo yum install python3 -y"]

    connection {
      host        = "${self.private_ip}"
      type        = "ssh"
      user        = var.amazon_linux_user
      private_key = "${file(local.key_path)}"
    }
  }

  provisioner "local-exec" {
    environment = {
      "ANS_PATH"                 = var.ans_path,
      "KEY_NAME"                 = var.key_name,
      "DEF_PATH"                 = var.def_path,
      "TF_WORKSPACE"             = terraform.workspace,
      "HOST_USER"                = var.amazon_linux_user,
      "SELF_PRIVATE_IP"          = self.private_ip,
      "MODULE_PATH"              = path.module
    }
    command = "${path.module}/scripts/web_server.sh"
  }

  depends_on = [aws_instance.graylog_server, aws_instance.mon_server, aws_instance.sonar_server]

}

