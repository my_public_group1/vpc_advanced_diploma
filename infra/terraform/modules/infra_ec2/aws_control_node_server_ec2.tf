# Запускаем инстанс control_node
resource "aws_instance" "my_cn" {
  # с выбранным образом ubuntu 
  ami                    = var.aws_ami_ubuntu_id
  # и размером (количество ЦПУ и памяти зависит от этой директивы) 
  instance_type          = "t2.micro"
  iam_instance_profile   = var.iam_instance_profile_cn_ec2_name
  vpc_security_group_ids = [aws_security_group.control_node.id]
  key_name               = var.key_name
  subnet_id              = var.private_subnet_ids[0]

  tags = {
    AMI =  "${var.aws_ami_ubuntu_id}"
    Name  = "Control Node Server (${terraform.workspace})"
  }

  depends_on = [aws_instance.graylog_server]

  connection {
      host        = "${self.private_ip}"
      type        = "ssh"
      user        = var.ubuntu_user
      private_key = "${file(local.key_path)}"
  }

  provisioner "file" {
    content = <<-EOT
      aws_region: ${var.region}
      bucket_name: ${var.bucket_name}
      bucket_key_path: ${var.bucket_key_path_version}
      aws_access_key_id_input: ${var.aws_access_key_id}
      aws_secret_access_key_input: ${var.aws_secret_access_key}
      aws_metaname: ${var.aws_metaname}
      graylog_endpoint: ${aws_instance.graylog_server.private_ip}
      repository: ${var.gitlab_repo}
      ansible_ssh_private_key_file: /www/${var.key_name}.pem
    EOT
    destination = "${var.srv_path}/vars.yml"
  }

  provisioner "file" {
    source      = "${path.root}/${var.key_name}.pem"
    destination = "${var.srv_path}/${var.key_name}.pem"
  }

  provisioner "remote-exec" {
    inline = [
      "sleep 10s", 
      "sudo apt-get update -y",
      "sleep 10s",
      "sudo apt-get install python3 -y",
      "sudo apt-get install python3-pip -y",
      "sudo mkdir ${var.var_www_path}",
      "sudo cp ${var.srv_path}/vars.yml ${var.var_www_path}/vars.yml",
      "sudo cp ${var.srv_path}/${var.key_name}.pem ${var.var_www_path}/${var.key_name}.pem",
      "rm -f ${var.srv_path}/vars.yml ${var.srv_path}/${var.key_name}.pem",
      "sudo chmod 400 ${var.var_www_path}/vars.yml ${var.var_www_path}/${var.key_name}.pem"
    ]
  }

  provisioner "local-exec" {
    environment = {
      "ANS_PATH"                 = var.ans_path,
      "KEY_NAME"                 = var.key_name,
      "DEF_PATH"                 = var.def_path,
      "TF_WORKSPACE"             = terraform.workspace,
      "HOST_USER"                = var.ubuntu_user,
      "SELF_PRIVATE_IP"          = self.private_ip,
      "GITLAB_API_TOKEN"         = var.gitlab_access_api_token,
      "MODULE_PATH"              = path.module
    }
    command = "${path.module}/scripts/control_node.sh" 
  }

  lifecycle {
    create_before_destroy = true
  }
}
