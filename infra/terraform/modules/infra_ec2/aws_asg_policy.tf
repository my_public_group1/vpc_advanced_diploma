resource "aws_autoscaling_policy" "asg_policy" {
  autoscaling_group_name = "${aws_autoscaling_group.web.name}"
  name                   = "asg-scale-policy-${terraform.workspace}"
  policy_type            = "TargetTrackingScaling"

  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }

    target_value = 40.0
  }
}
