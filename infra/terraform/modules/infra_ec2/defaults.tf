variable "var_www_path" {
  type    = string
  default = "/var/www"
}

variable "srv_path" {
  type    = string
  default = "/tmp"
}
