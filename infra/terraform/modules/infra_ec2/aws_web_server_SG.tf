resource "aws_security_group" "tfwebSG" {
  name        = "web-sg-${terraform.workspace}"
  vpc_id = var.vpc_id
  description = "Allows Ports to wordpress EC2 instance"

  dynamic "ingress" {
    for_each = var.server_ports.alb_server_tcp
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = var.allowed_cidr_blocks
    }
  }

  dynamic "ingress" {
    for_each = var.server_ports.web_server_tcp
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = [var.vpc_cidr_block]
    }
  }

  dynamic "ingress" {
    for_each = var.server_ports.web_server_udp
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "udp"
      cidr_blocks = [var.vpc_cidr_block]
    }
  }

  ingress {
    from_port       = var.ports.ssh_tcp[0]
    to_port         = var.ports.ssh_tcp[0]
    protocol        = "tcp"
    security_groups = [var.client_vpn_sg_id]
  }

  dynamic "egress" {
    for_each = var.server_ports.alb_server_tcp
    content {
      from_port   = egress.value
      to_port     = egress.value
      protocol    = "tcp"
      cidr_blocks = var.allowed_cidr_blocks
    }
  }

  egress {
    from_port   = var.ports.eph_tcp[0]
    to_port     = var.ports.eph_tcp[1]
    protocol    = "tcp"
    cidr_blocks = var.allowed_cidr_blocks
  }

  tags = {
    Name  = "Web Server Security Group (${terraform.workspace})"
  }

}
