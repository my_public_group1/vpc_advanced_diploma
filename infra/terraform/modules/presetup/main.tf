data "aws_caller_identity" "current" {}

data "aws_elb_service_account" "main" {}

data "aws_route53_zone" "zone" {
  name = "${var.route53_hosted_zone_name}"
}

data "aws_ami" "ubuntu" {
  owners      = ["099720109477"]
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
}

data "aws_ami" "acm_nitro_enclave" {
  owners      = ["679593333241"]
  most_recent = true
  filter {
    name   = "name"
    values = ["ACM-For-Nitro-Enclaves-1-2-0_2022-09-09T11-07-26.526Z-3f5ee4f8-1439-4bce-ac57-e794a4ca82f9"]
  }
}
