output "aws_caller" {
  value = data.aws_caller_identity.current.account_id
}

output "route53_zone_id" {
  value = data.aws_route53_zone.zone.zone_id
}

output "aws_ami_ubuntu_id" {
  value = data.aws_ami.ubuntu.id
}

output "aws_ami_acm_enclave_id" {
  value = data.aws_ami.acm_nitro_enclave.id
}

output "aws_elb_service_account_arn" {
  value = data.aws_elb_service_account.main.arn
}

