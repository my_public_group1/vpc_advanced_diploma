variable "zone_id" {
  type = string
}

variable "route53_hosted_zone_name" {
  type = string
}

variable "env_domain" {
  type = string
}

variable "web_public_ip" {
  type = string
}

variable "elb_dns_name" {
  type = string
}

variable "elb_zone_id" {
  type = string
}
