resource "aws_route53_record" "elb" {
  zone_id = "${var.zone_id}"
  name    = "${terraform.workspace}.${var.route53_hosted_zone_name}"
  type    = "A"

  alias {
    name                   = "${var.elb_dns_name}"
    zone_id                = "${var.elb_zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "web_services" {
  count   = length(local.web_services)
  zone_id = "${var.zone_id}"
  name    = "${local.web_services[count.index]}${var.env_domain}.${var.route53_hosted_zone_name}"
  type    = "A"
  ttl     = "300"
  records = ["${var.web_public_ip}"]
}

