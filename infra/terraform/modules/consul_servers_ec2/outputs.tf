output "consul_join_ips" {
  value = tolist(aws_instance.consul_server[*].private_ip)
}
