resource "aws_instance" "consul_server" {
  count = 3
  ami                    = var.aws_ami_ubuntu_id
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.consul_server.id]
  key_name = var.key_name
  subnet_id = var.private_subnet_ids[count.index]
  tags = {
    AMI =  "${var.aws_ami_ubuntu_id}"
    Name  = "Consul Server ${count.index} (${terraform.workspace})"
  }

  depends_on = [null_resource.remove_inventory_file]

  provisioner "remote-exec" {
    inline = ["sleep 15s", 
              "sudo apt-get update -y",
              "sleep 10s",
              "sudo apt-get install python3 -y",
              "sudo apt-get install python3-pip -y"]

    connection {
      host        = "${self.private_ip}"
      type        = "ssh"
      user        = var.ubuntu_user
      private_key = "${file(local.key_path)}"
    }
  }

  provisioner "local-exec" {
    environment = {
      "COUNT_INDEX"     = count.index,
      "PATH_KEY_FILE"   = local.key_path,
      "ANS_PATH"        = var.ans_path,
      "CONSUL_INV_FILE" = local.consul_inv_file,
      "HOST_USER"       = var.ubuntu_user,
      "SELF_PRIVATE_IP" = self.private_ip
    }
    command = "${path.module}/scripts/create_consul_inv.sh"
  }
}
