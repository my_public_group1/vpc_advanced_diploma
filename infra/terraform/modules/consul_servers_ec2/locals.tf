locals {
  consul_inv_file = "consul_servs_${terraform.workspace}.inv"
  key_path = "${path.root}/${var.key_name}.pem"
}
