resource "null_resource" "remove_inventory_file" {
  provisioner "local-exec" {
    environment = {
      "ANS_PATH"        = var.ans_path,
      "CONSUL_INV_FILE" = local.consul_inv_file 
    }
    command = "${path.module}/scripts/rm_and_create_consul_inv.sh"
  }
}


resource "null_resource" "run_ansible_consul_server" {
  provisioner "local-exec" {
    environment = {
      "ANS_PATH"                 = var.ans_path,
      "CONSUL_INV_FILE"          = local.consul_inv_file,
      "KEY_NAME"                 = var.key_name,
      "HOST_USER"                = var.ubuntu_user,
      "AWS_KEY_ID"               = var.aws_access_key_id,
      "AWS_KEY_VALUE"            = var.aws_secret_access_key,
      "BUCKET_NAME"              = var.bucket_name,
      "BUCKET_KEY_PATH"          = var.bucket_key_path_version,
      "META_NAME"                = var.aws_metaname,
      "REGION"                   = var.region,
      "DEFS_DIR"                 = var.defs_dir,
      "DEF_PATH"                 = var.def_path,
      "TF_WORKSPACE"             = terraform.workspace
    }
    command = "${path.module}/scripts/write_vars_and_run_ansible_on_consul.sh"
  }
  depends_on = [aws_instance.consul_server]
}
