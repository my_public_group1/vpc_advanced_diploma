#!/usr/bin/env bash

set -o nounset
set -o errexit

private_ip=$SELF_PRIVATE_IP

cat >>$ANS_PATH/$CONSUL_INV_FILE <<EOL
serv$COUNT_INDEX ansible_ssh_host=$private_ip ansible_ssh_user=$HOST_USER ansible_ssh_private_key_file=$PATH_KEY_FILE consul_bind_address=$private_ip consul_client_address='$private_ip 127.0.0.1'
EOL
