#!/usr/bin/env bash

set -o nounset
set -o errexit

def_path=$DEF_PATH
workspace=$TF_WORKSPACE

rm -fr $DEFS_DIR/
mkdir $DEFS_DIR/
cat >$DEFS_DIR/aws_cli.yml <<EOL
aws_access_key_id_input: $AWS_KEY_ID
aws_secret_access_key_input: $AWS_KEY_VALUE
EOL
cat >$DEFS_DIR/aws_s3.yml <<EOL
bucket_name: $BUCKET_NAME
bucket_key_path: $BUCKET_KEY_PATH
aws_metaname: $META_NAME
EOL
cat >$DEFS_DIR/aws_region.yml <<EOL
aws_region: $REGION
EOL
ansible-playbook -u $HOST_USER -i $ANS_PATH/$CONSUL_INV_FILE --private-key $KEY_NAME.pem $ANS_PATH/consul_servers/main.yml -e "def_path=$def_path tf_workspace=$workspace" -b -v
rm -f $ANS_PATH/$CONSUL_INV_FILE
