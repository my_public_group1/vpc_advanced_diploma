variable "aws_access_key_id" {
  type = string
}

variable "aws_secret_access_key" {
  type = string
}

variable "defs_dir" {
  type = string
}

variable "ports" {
  type = map(list(string))
}

variable "server_ports" {
  type = map(list(string))
}

variable "region" {
  type = string
}

variable "bucket_name" {
  type = string
}

variable "bucket_key_path_version" {
  type = string
}

variable "aws_metaname" {
  type = string
}

variable "allowed_cidr_blocks" {
  type = list
}

variable "ans_path" {
  type = string
}

variable "def_path" {
  type = string
}

variable "client_cidr_block" {
  description = "VPN CIDR block, must not overlap with VPC CIDR. Client cidr block must be at least a /22 range."
  type        = string
}

variable "ubuntu_user" {
  type = string
}

variable "aws_ami_ubuntu_id" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "private_subnet_ids" {
  type = list(string)
}

variable "key_name" {
  type = string
}

variable "vpc_cidr_block" {
  type = string
}

variable "client_vpn_sg_id" {
  type = string
}
