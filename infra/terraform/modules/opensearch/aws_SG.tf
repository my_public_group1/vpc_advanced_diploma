resource "aws_security_group" "opensearch" {
  name        = "opensearch-${var.os_domain}-sg-${terraform.workspace}"
  description = "SG for OpenSearch ${terraform.workspace}"
  vpc_id      = data.aws_vpc.vpc_data.id

  dynamic "ingress" {
    for_each = var.server_ports.os_domain_tcp
    content {
      from_port = ingress.value
      to_port   = ingress.value
      protocol  = "tcp"

      cidr_blocks = [
        data.aws_vpc.vpc_data.cidr_block,
      ]
    }
  }
}
