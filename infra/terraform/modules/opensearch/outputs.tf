output "aws_opensearch_endpoint" {
  value = aws_opensearch_domain.opensearch.endpoint
}
