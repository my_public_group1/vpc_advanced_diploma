resource "aws_opensearch_domain" "opensearch" {
  domain_name    = "${var.os_domain}"
  engine_version = "OpenSearch_2.5"

  cluster_config {
    instance_count         = 3
    instance_type          = "m4.large.search"
    zone_awareness_enabled = true
    zone_awareness_config {
      availability_zone_count = 3
    }
  }

  encrypt_at_rest {
    enabled = true
  }

  node_to_node_encryption {
    enabled = true
  }

  vpc_options {
    subnet_ids = [ for id in slice(var.private_subnet_ids, 0, 3) : id ] 
    security_group_ids = [aws_security_group.opensearch.id]
  }

  domain_endpoint_options {
    enforce_https = true
    tls_security_policy = "Policy-Min-TLS-1-2-2019-07"
  }


  advanced_security_options {
    enabled = true
    internal_user_database_enabled = true
    master_user_options {
      master_user_name = "${var.os_user}"
      master_user_password = "${var.os_pass}"
    }
  }

  ebs_options {
    ebs_enabled = true
    volume_size = 10
  }

  advanced_options = {
    "rest.action.multi.allow_explicit_index" = "true"
  }

  access_policies = data.aws_iam_policy_document.opensearch.json

  tags = {
    Domain = "${var.os_domain} ${terraform.workspace}"
  }

}
