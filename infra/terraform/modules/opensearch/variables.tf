variable "os_domain" {
  type = string
  default = "tf-opensearch"
}

variable "os_user" {
  type = string
  default = "admin"
}
  
variable "os_pass" {
  type = string
  default = "admin"
}

variable "server_ports" {
  type = map(list(string))
}

variable "region" {
  type = string
}

variable "caller_id" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "private_subnet_ids" {
  type = list(string)
}
