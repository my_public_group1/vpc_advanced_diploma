data "aws_vpc" "vpc_data" {
  id = var.vpc_id
}

data "aws_iam_policy_document" "opensearch" {
  statement {
    effect = "Allow"

    principals {
      type        = "*"
      identifiers = ["*"]
    }

    actions   = ["es:*"]
    resources = ["arn:aws:es:${var.region}:${var.caller_id}:domain/${var.os_domain}/*"]
  }
}
