resource "aws_subnet" "web_public_subnet" {
    vpc_id = aws_vpc.tfvpc.id
    cidr_block = "10.${var.vpc_number}.10.0/24"
    map_public_ip_on_launch = "true"
    availability_zone = data.aws_availability_zones.available.names[2]

    tags = {
        Name = "Web Public Subnet ${terraform.workspace}"
    }

    depends_on = [
        aws_vpc.tfvpc,
        aws_internet_gateway.tfgateway
    ]
}

resource "aws_subnet" "elb_public_subnet" {
    count = 2
    vpc_id = aws_vpc.tfvpc.id
    cidr_block = "10.${var.vpc_number}.${count.index + 11}.0/24"
    map_public_ip_on_launch = "true"
    availability_zone = data.aws_availability_zones.available.names[count.index]

    tags = {
        Name = "ELB Public Subnet${count.index} ${terraform.workspace}"
    }

    depends_on = [
        aws_vpc.tfvpc,
        aws_internet_gateway.tfgateway
    ]
}

resource "aws_subnet" "nat_public_subnet" {
    vpc_id = aws_vpc.tfvpc.id
    cidr_block = "10.${var.vpc_number}.13.0/24"
    map_public_ip_on_launch = "true"
    availability_zone = data.aws_availability_zones.available.names[0]

    tags = {
        Name = "NAT Public Subnet ${terraform.workspace}"
    }

    depends_on = [
        aws_vpc.tfvpc,
        aws_internet_gateway.tfgateway
    ]
}
