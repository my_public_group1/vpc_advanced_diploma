resource "aws_route_table_association" "tfpublicassociation" {
  count = length(local.pub_ngw_subnet_ids)
  subnet_id = local.pub_ngw_subnet_ids[count.index]
  route_table_id = aws_route_table.public_igw.id
}

resource "aws_route_table_association" "tfprivateassociation" {
  count = length(local.private_subnet_ids)
  subnet_id = local.private_subnet_ids[count.index]
  route_table_id = aws_route_table.private_ngw.id
}
