variable "vpc_number" {
  type = string
}

variable "client_cidr_block" {
  description = "VPN CIDR block, must not overlap with VPC CIDR. Client cidr block must be at least a /22 range."
  type        = string
}

variable "ports" {
  type = map(list(string))
}

variable "subnet_ports" {
  type = map(list(string))
}

variable "allowed_cidr_blocks" {
  type = list
}
