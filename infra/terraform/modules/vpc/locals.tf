locals {
  subnets = {
    tfpublic   = aws_subnet.web_public_subnet.id,
    tfprivate  = aws_subnet.infra_private_subnet.id,
  }
  asg_private_subnet_ids = tolist(aws_subnet.asg_private_subnet[*].id)
  elb_public_subnet_ids  = tolist(aws_subnet.elb_public_subnet[*].id)
  public_subnet_ids      = concat([local.subnets.tfpublic], local.elb_public_subnet_ids)
  private_subnet_ids     = concat([local.subnets.tfprivate], local.asg_private_subnet_ids)
  pub_ngw_subnet_ids     = concat(local.public_subnet_ids, [aws_subnet.nat_public_subnet.id])
}
