output "vpc_id" {
  value = aws_vpc.tfvpc.id
}

output "key_name" {
  value = aws_key_pair.deployer.key_name
#  sensitive = true
}

output "nat_gateway_ip" {
  value = aws_eip.nat_gateway.public_ip
}

output "private_subnet_ids" {
  value = local.private_subnet_ids
}

output "public_subnet_ids" {
  value = local.public_subnet_ids
}

output "vpc_cidr_block" {
  value = data.aws_vpc.selected.cidr_block
}
