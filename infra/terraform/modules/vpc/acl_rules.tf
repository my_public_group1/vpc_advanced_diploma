locals {
  range_web_public_tcp    = range(11, length(var.subnet_ports.web_public_tcp) + 11)
  range_web_public_udp    = range(51, length(var.subnet_ports.web_public_udp) + 51)
  range_asg_private_tcp   = range(91, length(var.subnet_ports.asg_private_tcp) + 91)
  range_asg_private_udp   = range(131, length(var.subnet_ports.asg_private_udp) + 131)
  range_infra_private_tcp = range(171, length(var.subnet_ports.infra_private_tcp) + 171)
  range_infra_private_udp = range(211, length(var.subnet_ports.infra_private_udp) + 211)
  range_alb_public_tcp    = range(251, length(var.subnet_ports.alb_public_tcp) + 251)
}

locals {
  web_tcp_rule_numbers   = slice(split(",", "%{ for num in local.range_web_public_tcp }${num}0,%{ endfor }"), 0, length(var.subnet_ports.web_public_tcp))
  web_udp_rule_numbers   = slice(split(",", "%{ for num in local.range_web_public_udp }${num}0,%{ endfor }"), 0, length(var.subnet_ports.web_public_udp))
  asg_tcp_rule_numbers   = slice(split(",", "%{ for num in local.range_asg_private_tcp }${num}0,%{ endfor }"), 0, length(var.subnet_ports.asg_private_tcp))
  asg_udp_rule_numbers   = slice(split(",", "%{ for num in local.range_asg_private_udp }${num}0,%{ endfor }"), 0, length(var.subnet_ports.asg_private_udp))
  infra_tcp_rule_numbers = slice(split(",", "%{ for num in local.range_infra_private_tcp }${num}0,%{ endfor }"), 0, length(var.subnet_ports.infra_private_tcp))
  infra_udp_rule_numbers = slice(split(",", "%{ for num in local.range_infra_private_udp }${num}0,%{ endfor }"), 0, length(var.subnet_ports.infra_private_udp))
  alb_tcp_rule_numbers   = slice(split(",", "%{ for num in local.range_alb_public_tcp }${num}0,%{ endfor }"), 0, length(var.subnet_ports.alb_public_tcp))
}

locals {
  web_acl_tcp_map   = zipmap(var.subnet_ports.web_public_tcp, local.web_tcp_rule_numbers)
  web_acl_udp_map   = zipmap(var.subnet_ports.web_public_udp, local.web_udp_rule_numbers)
  asg_acl_tcp_map   = zipmap(var.subnet_ports.asg_private_tcp, local.asg_tcp_rule_numbers)
  asg_acl_udp_map   = zipmap(var.subnet_ports.asg_private_udp, local.asg_udp_rule_numbers)
  infra_acl_tcp_map = zipmap(var.subnet_ports.infra_private_tcp, local.infra_tcp_rule_numbers)
  infra_acl_udp_map = zipmap(var.subnet_ports.infra_private_udp, local.infra_udp_rule_numbers)
  alb_acl_tcp_map   = zipmap(var.subnet_ports.alb_public_tcp, local.alb_tcp_rule_numbers)
}
