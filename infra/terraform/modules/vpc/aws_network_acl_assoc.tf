resource "aws_network_acl_association" "alb_public_acl_alloc" {
  count          = length(local.elb_public_subnet_ids)
  network_acl_id = aws_network_acl.alb_public_acl.id
  subnet_id      = local.elb_public_subnet_ids[count.index]
}

resource "aws_network_acl_association" "asg_private_acl_alloc" {
  count          = length(local.asg_private_subnet_ids)
  network_acl_id = aws_network_acl.asg_private_acl.id
  subnet_id      = local.asg_private_subnet_ids[count.index]
}

resource "aws_network_acl_association" "web_public_acl_alloc" {
  network_acl_id = aws_network_acl.web_public_acl.id
  subnet_id      = aws_subnet.web_public_subnet.id
}

resource "aws_network_acl_association" "infra_private_acl_alloc" {
  network_acl_id = aws_network_acl.infra_private_acl.id
  subnet_id      = aws_subnet.infra_private_subnet.id
}
