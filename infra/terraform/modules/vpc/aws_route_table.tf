resource "aws_route_table" "public_igw" {
  vpc_id = aws_vpc.tfvpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.tfgateway.id
  }

  tags = {
    Name = "Skill RT Public-IGW ${terraform.workspace}"
    description = "Route table for (in/out)bound traffic to vpc and public subnets"
  }

  depends_on = [aws_internet_gateway.tfgateway]
}

resource "aws_route_table" "private_ngw" {
  vpc_id = aws_vpc.tfvpc.id
  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_gw.id
  }

  tags = {
    Name = "Skill RT Private-NGW ${terraform.workspace}"
    description = "Route table for traffic from private subnets to Internet"
  }

}
