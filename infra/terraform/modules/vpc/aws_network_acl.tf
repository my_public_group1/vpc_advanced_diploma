resource "aws_network_acl" "alb_public_acl" {
  vpc_id = aws_vpc.tfvpc.id

  egress {
    protocol   = "tcp"
    rule_no    = 10
    action     = "allow"
    cidr_block = var.allowed_cidr_blocks[0]
    from_port  = var.ports.eph_tcp[0]
    to_port    = var.ports.eph_tcp[1]
  }

  dynamic "ingress" {
    for_each = local.alb_acl_tcp_map
    content {
      protocol   = "tcp"
      rule_no    = tonumber(ingress.value)
      action     = "allow"
      cidr_block = var.allowed_cidr_blocks[0]
      from_port  = ingress.key
      to_port    = ingress.key
    }
  }

  ingress {
    protocol   = "tcp"
    rule_no    = 20
    action     = "allow"
    cidr_block = data.aws_vpc.selected.cidr_block
    from_port  = var.ports.eph_tcp[0]
    to_port    = var.ports.eph_tcp[1]
  }

 
  tags = {
    Name = "ACL-ALB-PUBLIC-${terraform.workspace}"
  }
}

resource "aws_network_acl" "asg_private_acl" {
  vpc_id = aws_vpc.tfvpc.id

  egress {
    protocol   = "-1"
    rule_no    = 10
    action     = "allow"
    cidr_block = var.allowed_cidr_blocks[0]
    from_port  = 0
    to_port    = 0
  }

#  dynamic "ingress" {
#    for_each = local.asg_acl_tcp_map
#    content {
#      protocol   = "tcp"
#      rule_no    = tonumber(ingress.value)
#      action     = "allow"
#      cidr_block = data.aws_vpc.selected.cidr_block
#      from_port  = ingress.key
#      to_port    = ingress.key
#    }
#  }

  dynamic "ingress" {
    for_each = local.asg_acl_udp_map
    content {
      protocol   = "udp"
      rule_no    = tonumber(ingress.value)
      action     = "allow"
      cidr_block = data.aws_vpc.selected.cidr_block
      from_port  = ingress.key
      to_port    = ingress.key
    }
  }

  ingress {
    protocol   = "tcp"
    rule_no    = 90
    action     = "allow"
    cidr_block = data.aws_vpc.selected.cidr_block
    from_port  = var.ports.ssh_tcp[0]
    to_port    = var.ports.ssh_tcp[0]
  }

  ingress {
    protocol   = "tcp"
    rule_no    = 50
    action     = "allow"
    cidr_block = data.aws_vpc.selected.cidr_block
    from_port  = var.ports.https_tcp[0]
    to_port    = var.ports.https_tcp[0]
  }

  ingress {
    protocol   = "tcp"
    rule_no    = 20
    action     = "allow"
    cidr_block = var.allowed_cidr_blocks[0]
    from_port  = var.ports.eph_tcp[0]
    to_port    = var.ports.eph_tcp[1]
  }

  tags = {
    Name = "ACL-ASG-PRIVATE-${terraform.workspace}"
  }
}

resource "aws_network_acl" "web_public_acl" {
  vpc_id = aws_vpc.tfvpc.id

  egress {
    protocol   = "-1"
    rule_no    = 10
    action     = "allow"
    cidr_block = var.allowed_cidr_blocks[0]
    from_port  = 0
    to_port    = 0
  }

  dynamic "ingress" {
    for_each = local.alb_acl_tcp_map
    content {
      protocol   = "tcp"
      rule_no    = tonumber(ingress.value)
      action     = "allow"
      cidr_block = var.allowed_cidr_blocks[0]
      from_port  = ingress.key
      to_port    = ingress.key
    }
  }

#  dynamic "ingress" {
#    for_each = local.web_acl_tcp_map
#    content {
#      protocol   = "tcp"
#      rule_no    = tonumber(ingress.value)
#      action     = "allow"
#      cidr_block = data.aws_vpc.selected.cidr_block
#      from_port  = ingress.key
#      to_port    = ingress.key
#    }
#  }

  dynamic "ingress" {
    for_each = local.web_acl_udp_map
    content {
      protocol   = "udp"
      rule_no    = tonumber(ingress.value)
      action     = "allow"
      cidr_block = data.aws_vpc.selected.cidr_block
      from_port  = ingress.key
      to_port    = ingress.key
    }
  }


  ingress {
    protocol   = "tcp"
    rule_no    = 90
    action     = "allow"
    cidr_block = data.aws_vpc.selected.cidr_block
    from_port  = var.ports.ssh_tcp[0]
    to_port    = var.ports.ssh_tcp[0]
  }

  ingress {
    protocol   = "tcp"
    rule_no    = 20
    action     = "allow"
    cidr_block = var.allowed_cidr_blocks[0]
    from_port  = var.ports.eph_tcp[0]
    to_port    = var.ports.eph_tcp[1]
  }
  
  tags = {
    Name = "ACL-WEB-PUBLIC-${terraform.workspace}"
  }
}

resource "aws_network_acl" "infra_private_acl" {
  vpc_id = aws_vpc.tfvpc.id

  egress {
    protocol   = "-1"
    rule_no    = 10
    action     = "allow"
    cidr_block = var.allowed_cidr_blocks[0]
    from_port  = 0
    to_port    = 0
  }

  ingress {
    protocol   = "tcp"
    rule_no    = 20
    action     = "allow"
    cidr_block = var.allowed_cidr_blocks[0]
    from_port  = var.ports.eph_tcp[0]
    to_port    = var.ports.eph_tcp[1]
  }

  ingress {
    protocol   = "udp"
    rule_no    = 30
    action     = "allow"
    cidr_block = data.aws_vpc.selected.cidr_block
    from_port  = var.ports.eph_tcp[0]
    to_port    = var.ports.eph_tcp[1]
  }

  ingress {
    protocol   = "tcp"
    rule_no    = 90
    action     = "allow"
    cidr_block = data.aws_vpc.selected.cidr_block
    from_port  = var.ports.ssh_tcp[0]
    to_port    = var.ports.ssh_tcp[0]
  }

  ingress {
    protocol   = "tcp"
    rule_no    = 50
    action     = "allow"
    cidr_block = data.aws_vpc.selected.cidr_block
    from_port  = var.ports.https_tcp[0]
    to_port    = var.ports.https_tcp[0]
  }


#not enough acl rules limit. Need increase quota.
#  dynamic "ingress" {
#    for_each = local.infra_acl_tcp_map
#    content {
#      protocol   = "tcp"
#      rule_no    = tonumber(ingress.value)
#      action     = "allow"
#      cidr_block = data.aws_vpc.selected.cidr_block
#      from_port  = ingress.key
#      to_port    = ingress.key
#    }
#  }

#  dynamic "ingress" {
#    for_each = local.infra_acl_udp_map
#    content {
#      protocol   = "udp"
#      rule_no    = tonumber(ingress.value)
#      action     = "allow"
#      cidr_block = data.aws_vpc.selected.cidr_block
#      from_port  = ingress.key
#      to_port    = ingress.key
#    }
#  }


  tags = {
    Name = "ACL-INFRA-PRIVATE-${terraform.workspace}"
  }
}
