resource "aws_subnet" "infra_private_subnet" {
    vpc_id = aws_vpc.tfvpc.id
    cidr_block = "10.${var.vpc_number}.14.0/24"
    availability_zone = data.aws_availability_zones.available.names[2]

    tags = {
        Name = "Skill Infra Private Subnet ${terraform.workspace}"
    }

    depends_on = [
        aws_vpc.tfvpc
    ]
}

resource "aws_subnet" "asg_private_subnet" {
    count = 2
    vpc_id = aws_vpc.tfvpc.id
    cidr_block = "10.${var.vpc_number}.${count.index + 15}.0/24"
    availability_zone = data.aws_availability_zones.available.names[count.index]

    tags = {
        Name = "ASG Private Subnet${count.index} ${terraform.workspace}"
    }

    depends_on = [
        aws_vpc.tfvpc
    ]
}
