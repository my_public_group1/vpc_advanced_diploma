resource "null_resource" "remove_defs" {
  provisioner "local-exec" {
    command = "rm -fr ${var.defs_dir}"    
  }
}

