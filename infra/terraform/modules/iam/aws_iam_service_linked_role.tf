resource "aws_iam_service_linked_role" "asg" {
  aws_service_name = "autoscaling.amazonaws.com"
  description      = "A service linked role for autoscaling ${terraform.workspace}"
  custom_suffix = "${terraform.workspace}"
  provisioner "local-exec" {
    command = "sleep 10"
  }
}
