#!/usr/bin/env bash

aws ec2 --region $REGION disassociate-enclave-certificate-iam-role --certificate-arn $CERT_ARN --role-arn $ROLE_ARN

