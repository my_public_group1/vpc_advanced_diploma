#!/usr/bin/env bash
sleep 20s
aws ec2 --region $1 associate-enclave-certificate-iam-role --certificate-arn $2 --role-arn $3

