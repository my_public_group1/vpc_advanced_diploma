resource "null_resource" "role_arn_test" {
  provisioner "local-exec" {
    command = "echo Done!"
  }
  depends_on = [
    aws_iam_role.enclave_acm_access_role,
  ]
}

resource "null_resource" "disassociate-enclave-certificate-iam-role" {
  triggers = {
    region = var.region
    cert_arn = var.certificate_arn
    role_arn = aws_iam_role.enclave_acm_access_role.arn
  }
  provisioner "local-exec" {
    when = destroy
    environment = merge(
      { "REGION"   = self.triggers.region },
      { "CERT_ARN" = self.triggers.cert_arn },
      { "ROLE_ARN" = self.triggers.role_arn })
    command = "${path.module}/scripts/disassociate-enclave-certificate-iam-role.sh"
  }
  depends_on = [
    aws_iam_role.enclave_acm_access_role,
  ]
}
