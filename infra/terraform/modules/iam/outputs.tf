output "iam_instance_profile_enclave_acm_name" {
  value = aws_iam_instance_profile.enclave_acm_profile.name
}

output "iam_instance_profile_cn_ec2_name" {
  value = aws_iam_instance_profile.cn_ec2_profile.name
}

output "iam_service_linked_role_asg_arn" {
  value = aws_iam_service_linked_role.asg.arn
}
