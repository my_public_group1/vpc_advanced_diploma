variable "region" {
  type = string
}

variable "caller_id" {
  type = string
}

variable "certificate_arn" {
  type = string
}

variable "aws_elb_service_account_arn" {
  type = string
}

variable "bucket_name" {
  type = string
}

variable "bucket_key_path_lb_logs" {
  type = string
}


