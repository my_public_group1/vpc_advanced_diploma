resource "aws_iam_role" "cn_ec2_access_role" {
  name               = "cn-ec2-role-${terraform.workspace}"
  assume_role_policy = data.aws_iam_policy_document.acm_role.json
}

resource "aws_iam_role" "enclave_acm_access_role" {
  name               = "enclave-acm-role-${terraform.workspace}"
  assume_role_policy = data.aws_iam_policy_document.acm_role.json
}

resource "aws_iam_instance_profile" "cn_ec2_profile" {
  name               = "cn-ec2-profile-${terraform.workspace}"                         
  role               = aws_iam_role.cn_ec2_access_role.name
}

resource "aws_iam_instance_profile" "enclave_acm_profile" {
  name               = "enclave-acm-profile-${terraform.workspace}"
  role               = aws_iam_role.enclave_acm_access_role.name
}

resource "aws_iam_policy" "ec2_full_policy" {
  name               = "ec2-full-policy-${terraform.workspace}"
  description        = "EC2 full access policy"
  policy             = data.aws_iam_policy_document.ec2_full_policy.json
}

resource "aws_iam_policy" "cloudwatch_policy" {
  name               = "cloudwatch-policy-${terraform.workspace}"
  description        = "CloudWatch full access policy"
  policy             = data.aws_iam_policy_document.cloudwatch_policy.json
}

resource "aws_iam_policy" "acm_role_policies" {
  name               = "acm-role-policy-${terraform.workspace}"
  description        = "Nitro Enclave Policies"
  policy             = data.aws_iam_policy_document.acm_role_policies.json
}

resource "aws_iam_policy" "acm_enclave_policies" {
  name               = "nitro-enclave-policy-${terraform.workspace}"
  description        = "General Nitro Enclave Policies"
  policy             = data.aws_iam_policy_document.acm_enclave.json
}

resource "aws_iam_role_policy_attachment" "ec2_attach" {
  role               = aws_iam_role.cn_ec2_access_role.name
  policy_arn         = aws_iam_policy.ec2_full_policy.arn
}

resource "aws_iam_role_policy_attachment" "cloudwatch_attach" {
  role               = aws_iam_role.cn_ec2_access_role.name
  policy_arn         = aws_iam_policy.cloudwatch_policy.arn
}

resource "aws_iam_role_policy_attachment" "acm_role_policies" {
  role               = aws_iam_role.enclave_acm_access_role.name
  policy_arn         = aws_iam_policy.acm_role_policies.arn
}

resource "aws_iam_role_policy_attachment" "acm_enclave_policies" {
  role               = aws_iam_role.enclave_acm_access_role.name
  policy_arn         = aws_iam_policy.acm_enclave_policies.arn
}

resource "aws_s3_bucket_policy" "load_balancer_access_logs_bucket_policy" {
  bucket             = var.bucket_name
  policy             = data.aws_iam_policy_document.s3_lb_write.json
}
