data "external" "associate_enclave_certificate_iam_role_json" {
  program = [
    "bash",
    "${path.module}/scripts/associate-enclave-certificate-iam-role.sh",
    "${var.region}",
    "${var.certificate_arn}",
    "${aws_iam_role.enclave_acm_access_role.arn}"
  ]
  depends_on = [
    aws_iam_role.enclave_acm_access_role,
    null_resource.role_arn_test
  ]
}
