data "aws_iam_policy_document" "acm_role" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }

    actions   = [
      "sts:AssumeRole"
    ]
  }
}

data "aws_iam_policy_document" "acm_enclave" {
  statement {
    effect = "Allow"

    actions   = [
      "ec2:AssociateEnclaveCertificateIamRole",
      "ec2:GetAssociatedEnclaveCertificateIamRoles",
      "ec2:DisassociateEnclaveCertificateIamRole"
    ]
    resources = [
      "arn:aws:acm:${var.region}:${var.caller_id}:certificate/*",
      "arn:aws:iam::${var.caller_id}:role/*"
    ]
  }
}


data "aws_iam_policy_document" "acm_role_policies" {
  statement {
    effect = "Allow"
    actions   = [
      "s3:GetObject"
    ]
    resources = [
      "arn:aws:s3:::${local.enclave_cert_map.CertificateS3BucketName}/*"
    ]
  }
  statement {
    sid = "VisualEditor0"
    effect = "Allow"
    actions   = [
      "kms:Decrypt"
    ]
    resources = [
      "arn:aws:kms:${var.region}:*:key/${local.enclave_cert_map.EncryptionKmsKeyId}"
    ]
  }
  statement {
    effect = "Allow"
    actions   = [
      "iam:GetRole"
    ]
    resources = [
      aws_iam_role.enclave_acm_access_role.arn
    ]
  }
}

data "aws_iam_policy_document" "ec2_full_policy" {
  statement {
    effect = "Allow"
    actions   = [
      "ec2:*"
    ]
    resources = [
      "*"
    ]
  }
  statement {
    effect = "Allow"
    actions   = [
      "elasticloadbalancing:*"
    ]
    resources = [
      "*"
    ]
  }
  statement {
    effect = "Allow"
    actions   = [
      "cloudwatch:*"
    ]
    resources = [
      "*"
    ]
  }
  statement {
    effect = "Allow"
    actions   = [
      "autoscaling:*"
    ]
    resources = [
      "*"
    ]
  }
  statement {
    effect = "Allow"
    actions   = [
      "iam:CreateServiceLinkedRole"
    ]
    resources = [
      "*"
    ]
    condition {
      test     = "StringEquals"
      variable = "iam:AWSServiceName"

      values = [
        "autoscaling.amazonaws.com",
        "ec2scheduled.amazonaws.com",
        "elasticloadbalancing.amazonaws.com",
        "spot.amazonaws.com",
        "spotfleet.amazonaws.com",
        "transitgateway.amazonaws.com"
      ]
    }
  }
}

data "aws_iam_policy_document" "cloudwatch_policy" {
  statement {
    sid = "Stmt1674249227793"
    effect = "Allow"
    actions   = [
      "tag:GetResources",
      "cloudwatch:GetMetricData",
      "cloudwatch:GetMetricStatistics",
      "cloudwatch:ListMetrics",
      "ec2:DescribeTags",
      "ec2:DescribeInstances",
      "ec2:DescribeRegions",
      "ec2:DescribeTransitGateway*",
      "apigateway:GET",
      "dms:DescribeReplicationInstances",
      "dms:DescribeReplicationTasks"
    ]
    resources = [
      "*"
    ]
  }
}

data "aws_iam_policy_document" "s3_lb_write" {
  statement {
    effect = "Allow"
    principals {
      identifiers = [var.aws_elb_service_account_arn]
      type = "AWS"
    }

    actions = ["s3:PutObject"]

    resources = [
      "arn:aws:s3:::${var.bucket_name}/${var.bucket_key_path_lb_logs}/*"
    ]
  }
}
