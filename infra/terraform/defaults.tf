variable "allowed_cidr_blocks" {
  type = list
  default = ["0.0.0.0/0"]
}

variable "def_path" {
  type = string
  default = "general_defaults"
}

variable "ans_path" {
  type = string
  default = "../ansible"
}

variable "ubuntu_user" {
  type = string
  default = "ubuntu"
}

variable "amazon_linux_user" {
  type = string
  default = "ec2-user"
}

variable "bucket_key_path_lb_logs" {
  type = string
  default = "lb-logs"
}

