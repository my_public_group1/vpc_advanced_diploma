locals {
  predefs = {
    vpc_number              = terraform.workspace == "app" ? "0" : "1"
  }
}

locals {
  defs = {
    vpn_client_cidr_block   = "10.${local.predefs.vpc_number}.0.0/22"
    vpn_cert_issuer         = "skill.internal"
    vpn_cert_server_name    = "skill-${terraform.workspace}"
    vpn_endpoint_name       = "Skill VPN Endpoint-${terraform.workspace}"
    env_domain              = terraform.workspace == "app" ? "" : "-test"
    defs_dir                = "${var.ans_path}/${var.def_path}_${terraform.workspace}"
  }
}
