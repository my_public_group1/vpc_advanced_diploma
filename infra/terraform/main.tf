module "presetup" {
  source                    = "./modules/presetup"
  route53_hosted_zone_name  = local.inputs.common_domain
}

module "acm" {
  depends_on                = [module.presetup]
  source                    = "./modules/acm"
  domain_name               = local.inputs.common_domain
  subject_alternative_names = ["*.${local.inputs.common_domain}"]
  zone_id                   = module.presetup.route53_zone_id
}

module "vpc" {
  source                    = "./modules/vpc"
  vpc_number                = local.predefs.vpc_number
  client_cidr_block         = local.defs.vpn_client_cidr_block
  ports                     = local.ports
  subnet_ports              = local.subnet_ports
  allowed_cidr_blocks       = var.allowed_cidr_blocks
}

module "client_vpn" {
  depends_on                = [module.vpc]
  source                    = "./modules/client_vpn"
  cert_issuer               = local.defs.vpn_cert_issuer
  cert_server_name          = local.defs.vpn_cert_server_name
  client_cidr_block         = local.defs.vpn_client_cidr_block
  vpn_name                  = local.defs.vpn_endpoint_name
  env                       = terraform.workspace
  region                    = var.aws_region
  vpc_id                    = module.vpc.vpc_id
  private_subnet_ids        = module.vpc.private_subnet_ids
}

module "opensearch" {
  depends_on                = [module.presetup, module.vpc]
  source                    = "./modules/opensearch"
  os_domain                 = join("-", [local.inputs.os_domain, terraform.workspace])
  os_user                   = local.inputs.opensearch_user
  os_pass                   = local.inputs.opensearch_password
  server_ports              = local.server_ports
  region                    = var.aws_region
  caller_id                 = module.presetup.aws_caller
  vpc_id                    = module.vpc.vpc_id
  private_subnet_ids        = module.vpc.private_subnet_ids
}

module "iam" {
  depends_on                  = [module.presetup, module.acm]
  source                      = "./modules/iam"
  region                      = var.aws_region
  caller_id                   = module.presetup.aws_caller
  aws_elb_service_account_arn = module.presetup.aws_elb_service_account_arn
  certificate_arn             = module.acm.acm_certificate_arn
  bucket_name                 = var.bucket_name
  bucket_key_path_lb_logs     = var.bucket_key_path_lb_logs
}

module "consul_servers_ec2" {
  depends_on                = [module.presetup, module.vpc, module.client_vpn]
  source                    = "./modules/consul_servers_ec2"
  aws_access_key_id         = local.inputs.aws_access_key_id
  aws_secret_access_key     = local.inputs.aws_secret_access_key
  defs_dir                  = local.defs.defs_dir
  client_cidr_block         = local.defs.vpn_client_cidr_block
  ports                     = local.ports
  server_ports              = local.server_ports
  region                    = var.aws_region
  bucket_name               = var.bucket_name
  bucket_key_path_version   = var.bucket_key_path_version
  aws_metaname              = var.meta_name
  allowed_cidr_blocks       = var.allowed_cidr_blocks
  ans_path                  = var.ans_path
  def_path                  = var.def_path
  ubuntu_user               = var.ubuntu_user
  aws_ami_ubuntu_id         = module.presetup.aws_ami_ubuntu_id
  vpc_id                    = module.vpc.vpc_id
  private_subnet_ids        = module.vpc.private_subnet_ids
  key_name                  = module.vpc.key_name
  vpc_cidr_block            = module.vpc.vpc_cidr_block
  client_vpn_sg_id          = module.client_vpn.client_vpn_sg_id
}

module "infra_ec2" {
  depends_on                            = [module.presetup, module.vpc, module.acm, module.client_vpn, module.opensearch, module.iam, module.consul_servers_ec2]
  source                                = "./modules/infra_ec2"
  route53_hosted_zone_name              = local.inputs.common_domain
  aws_access_key_id                     = local.inputs.aws_access_key_id
  aws_secret_access_key                 = local.inputs.aws_secret_access_key
  gitlab_repo                           = local.inputs.gitlab_repo
  gitlab_token                          = local.inputs.gitlab_token
  gitlab_access_api_token               = local.inputs.gitlab_access_api_token
  os_user                               = local.inputs.opensearch_user
  os_pass                               = local.inputs.opensearch_password
  grafana_user                          = local.inputs.grafana_user
  grafana_pass                          = local.inputs.grafana_pass
  pagerduty_user_url                    = local.inputs.pagerduty_user_url
  pagerduty_user_key                    = local.inputs.pagerduty_user_key
  graylog_secret                        = local.inputs.graylog_secret
  graylog_password                      = local.inputs.graylog_password
  sonar_password                        = local.inputs.sonar_password
  psql_password                         = local.inputs.psql_password
  env_domain                            = local.defs.env_domain
  defs_dir                              = local.defs.defs_dir
  client_cidr_block                     = local.defs.vpn_client_cidr_block
  ports                                 = local.ports
  server_ports                          = local.server_ports
  region                                = var.aws_region
  bucket_name                           = var.bucket_name
  bucket_key_path_version               = var.bucket_key_path_version
  bucket_key_path_lb_logs               = var.bucket_key_path_lb_logs
  aws_metaname                          = var.meta_name
  allowed_cidr_blocks                   = var.allowed_cidr_blocks
  ans_path                              = var.ans_path
  def_path                              = var.def_path
  ubuntu_user                           = var.ubuntu_user
  amazon_linux_user                     = var.amazon_linux_user
  aws_ami_acm_enclave_id                = module.presetup.aws_ami_acm_enclave_id
  aws_ami_ubuntu_id                     = module.presetup.aws_ami_ubuntu_id
  vpc_id                                = module.vpc.vpc_id
  private_subnet_ids                    = module.vpc.private_subnet_ids
  public_subnet_ids                     = module.vpc.public_subnet_ids
  key_name                              = module.vpc.key_name
  vpc_cidr_block                        = module.vpc.vpc_cidr_block
  certificate_arn                       = module.acm.acm_certificate_arn
  client_vpn_sg_id                      = module.client_vpn.client_vpn_sg_id
  aws_opensearch_endpoint               = module.opensearch.aws_opensearch_endpoint
  iam_service_linked_role_asg_arn       = module.iam.iam_service_linked_role_asg_arn
  iam_instance_profile_cn_ec2_name      = module.iam.iam_instance_profile_cn_ec2_name
  iam_instance_profile_enclave_acm_name = module.iam.iam_instance_profile_enclave_acm_name
  consul_join_ips                       = module.consul_servers_ec2.consul_join_ips
}

module "route53" {
  depends_on                = [module.presetup, module.infra_ec2]
  source                    = "./modules/route53"
  route53_hosted_zone_name  = local.inputs.common_domain
  env_domain                = local.defs.env_domain
  zone_id                   = module.presetup.route53_zone_id
  web_public_ip             = module.infra_ec2.web_public_ip
  elb_dns_name              = module.infra_ec2.elb_dns_name
  elb_zone_id               = module.infra_ec2.elb_zone_id
}

module "remove_defs" {
  depends_on = [module.route53]
  source     = "./modules/remove_defs"
  defs_dir   = local.defs.defs_dir
}
