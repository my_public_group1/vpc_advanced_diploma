aws_region              = "us-west-2"
bucket_name             = "skill-site-app-s3-state"
bucket_key_path_version = "kv-store/release_version"
meta_name               = "release-version"
