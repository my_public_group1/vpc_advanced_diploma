# 2. VPN EndPoint

Date: 2023-05-03

## Status

Accepted

## Context

Our infrastructure has private subnets and instances that do not have public IP addresses. To have access to these resources, we need to create a bastion host or VPN Server.

## Decision

To solve this problem, the creation of [AWS Client VPN](https://docs.aws.amazon.com/vpn/latest/clientvpn-admin/what-is.html) was chosen. We create an _AWS Client VPN EndPoint_, also add inbound Client VPN authorization rules for all subnets (private and public), and associate the VPN EndPoint with public subnets.

When building the infrastructure for each environment (test and app), a directory is created with certificates and keys for connecting to the VPN EndPoint, and the OpenVPN config is also downloaded, which is needed to connect using the client. Connection occurs automatically using a bash script launched from Terraform.

The paths to the OpenVPN configuration file and the directory with certificates and keys are shown in the table below:

| Object | Path App (env) | Path Test (env) |
| --- | ----------- | ----------- |
| OpenVPN Config File | client.aws.app.ovpn | client.aws.test.ovpn |
| Certs and Keys Dir | pki_app/ | pki_test/ |
 
All instances on public and private subnets, except autoscale group instances, have ingress rules such that ssh connections are only available from VPN clients.

## Consequences


Implementing AWS Client VPN improves infrastructure security by creating an encrypted connection from the admin device to all infrastructure resources.
