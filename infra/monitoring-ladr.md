# 1. Monitoring system

Date: 2023-05-02

## Status

Accepted

## Context

We need to create a monitoring system for our server infrastructure deployed on [Amazon AWS](https://aws.amazon.com/).

The monitoring system should be based on Prometheus and related tools.

## Decision

A separate Amazon AWS instance is created for the monitoring system, on which the following tools are installed using Ansible roles:

| Tool | Ansible Role |
| --- | ----------- |
| [Prometheus](https://prometheus.io/) | [cloudalchemy.prometheus](https://github.com/cloudalchemy/ansible-prometheus) |
| [VictoriaMetrics](https://victoriametrics.com/) | [victoriametrics.single](https://github.com/VictoriaMetrics/ansible-playbooks) |
| [Alertmanager](https://prometheus.io/download/#alertmanager) | [cloudalchemy.alertmanager](https://github.com/cloudalchemy/ansible-alertmanager) |
| [Grafana](https://grafana.com/) | [cloudalchemy.grafana](https://github.com/cloudalchemy/ansible-grafana) |

Various exporters for Prometheus have also been installed on the following servers using Ansible roles:

| Server | Exporter | Ansible Role |
| ----------- | ----------- | ----------- |
| Consul Servers | None | None |
| ----------- | ----------- | ----------- |
| Graylog Server | [Node Exporter](https://prometheus.io/download/#node_exporter) | [cloudalchemy.node_exporter](https://github.com/cloudalchemy/ansible-node-exporter) |
| ----------- | ----------- | ----------- |
| AWS ASG Servers | [Node Exporter](https://prometheus.io/download/#node_exporter) | [cloudalchemy.node_exporter](https://github.com/cloudalchemy/ansible-node-exporter) |
|  | cAdvisor | [ome.cadvisor](https://github.com/ome/ansible-role-cadvisor) |
| ----------- | ----------- | ----------- |
| Monitoring Server | [Node Exporter](https://prometheus.io/download/#node_exporter) | [cloudalchemy.node_exporter](https://github.com/cloudalchemy/ansible-node-exporter) |
| ----------- | ----------- | ----------- |
| Control Node Server (gitlab-runner) | [Node Exporter](https://prometheus.io/download/#node_exporter) | [cloudalchemy.node_exporter](https://github.com/cloudalchemy/ansible-node-exporter) |
|  | cAdvisor | [ome.cadvisor](https://github.com/ome/ansible-role-cadvisor) |
|  | CloudWatch Exporter | [YACE](https://github.com/nerdswords/yet-another-cloudwatch-exporter) |
| ----------- | ----------- | ----------- |
| SonarQube Server | [Node Exporter](https://prometheus.io/download/#node_exporter) | [cloudalchemy.node_exporter](https://github.com/cloudalchemy/ansible-node-exporter) |
|  | cAdvisor | [ome.cadvisor](https://github.com/ome/ansible-role-cadvisor) |
| ----------- | ----------- | ----------- |
| AWS Enclave Web Server | [Node Exporter](https://prometheus.io/download/#node_exporter) | [cloudalchemy.node_exporter](https://github.com/cloudalchemy/ansible-node-exporter) |

Prometheus also collects metrics from the GO app from the /metrics path.

The scheme of interaction of monitoring tools is shown in the figure below:
![scheme](../pictures/monitoring_scheme.png)

1. Prometheus connects to Consul Server to get ip-addresses and ports on which Consul services are registered;
2. Next, Prometheus collects metrics from all targets and sends alerts using the AlertManager to [PagerDuty](https://www.pagerduty.com/). Also, Prometheus does not store all the collected data itself, it records them remotely on a lighter VictoriaMetrics tool;
3. Grafana collects all metrics from VictoriaMetrics;

Grafana has dashboards for the following metrics:
1. Node Exporter metrics;
2. cAdvisor metrics;
3. GO app metrics;
4. Alertmanager metrics;
5. Graylog metrics;
6. SonarQube metrics.

Monitoring tools can be accessed publicly through the following domains:

| Tool | Domain App (env) | Domain Test (env) |
| --- | ----------- | ----------- |
| Prometheus | https://prom.example.com/ | https://prom-test.example.com/ |
| Grafana | https://grafana.example.com/ | https://grafana-test.example.com/ |
| Alertmanager | https://am.example.com/ | https://am-test.example.com/ |
| VictoriaMetrics | https://victoria.example.com/ | https://victoria-test.example.com/ |

## Consequences

After creating a monitoring system, we will be able to timely monitor the status of our servers and services installed on it, which can help fix the problem in a short time.
