# 3. Release Version

Date: 2023-05-03

## Status

Accepted

## Context

We need to store the version of the service, and also update it every time we update the code in the "main" branch.

## Decision

Our infrastructure initially creates an [AWS S3 bucket](https://aws.amazon.com/s3/) that is used to store Terraform states. It was decided that when an _AWS S3 bucket_ is created, an empty object is created with key-value metadata. This object is initialized with the _key_ `release-version` and the _value_ `1`.

Further, when creating _Consul_ servers, the ansible role is launched, which gets the value of `release-version` key from the bucket and writes it to the _Consul key-value_ storage with the `release_version` key.

Let's consider the case when automatic scale group instances are created or code is updated in the `uat-*` test branch. In this case, the ansible role is launched, which gets the value of the current version from the _Consul kv-storage_, runs _docker-compose_ with the service, and names the _docker-container_ with the current version.

In the case when the code is updated in the `main` branch, the version value is read from the _Consul kv-storage_, incremented by `1`, and the new value is written to the _AWS S3 bucket_ and _Consul kv-storage_. The _docker-container_ is created with the updated version in the name.

## Consequences

This solution allows you to better store the current version, because if the main infrastructure is destroyed, the current version will continue to be stored in the AWS S3 bucket.
