# 5. Log collection system.

Date: 2023-05-04

## Status

Accepted

## Context

We need to create a log collection system for our server infrastructure deployed on [Amazon AWS](https://aws.amazon.com/).

The log collection system should be based on [AWS OpenSearch Domain](https://aws.amazon.com/opensearch-service/), [GrayLog](https://www.graylog.org/) and [Fluent-Bit](https://fluentbit.io/).

## Decision

The solution was to create an _AWS OpenSearch Domain_ with three _nodes_, one each in a separate _accessible zone_ and in a separate _private subnet_

Separately, a _GrayLog_ server is created and configured on an _EC2_ instance. _GrayLog_ creates a special input to which various log collectors can send logs. _GrayLog_ itself allows you to change the collected logs to a more appropriate type. _GrayLog_ then writes all the logs to the _AWS OpenSearch Domain_ search engine.

Absolutely all instances in our infrastructure, except for the _GrayLog_ server, have the _Fluent-Bit_ log collector installed. It is configured to send various logs to a special _Graylog_ server input.

The general scheme of interaction between the elements of the log collection system is presented below:
![scheme](../pictures/logs-scheme.png)


## Consequences

This system allows you to collect various logs, automatically change their type, as well as make quick and high-quality analysis and search for the necessary information among a huge number of logs. This leads to increased system security, as it becomes possible to detect and respond to various security incidents faster.
