# 4. Virtual Private Cloud (VPC)

Date: 2023-05-03

## Status

Accepted

## Context

To create a more secure infrastructure, we need to create public and private subnets for access control.

## Decision

It was decided to create a separate [AWS VPC](https://docs.aws.amazon.com/vpc/latest/userguide/what-is-amazon-vpc.html) for each environment with 4 public and 3 private subnets. 

The following network elements are also created:
- [AWS Internet Gateway](https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Internet_Gateway.html)
- [AWS NAT Gateway](https://docs.aws.amazon.com/vpc/latest/userguide/vpc-nat-gateway.html)
- Two [AWS Routing Tables](https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Route_Tables.html) (IGW and NGW respectively)

Public subnets use a routing table directed to the IGW. This is necessary for communication between public instances and the Internet.

Private subnets use a routing table directed to the NGW. This is necessary in order for private instances to have Internet access for updating and installing software packages. With NGW, all requests from private subnets are sent as if from a public instance to IGW.

The list of subnet and resource ratios is shown below.

| Type of subnet | Subnet | Resource |
| --- | ----------- | ----------- |
| Public | Web Public Subnet | [AWS Enclave Web Server](https://aws.amazon.com/ec2/nitro/nitro-enclaves/) |
|  | 2 ELB Public Subnets | 2 [AWS Application Load Balancers](https://aws.amazon.com/elasticloadbalancing/application-load-balancer/) |
|  | NAT Public Subnet | [AWS NAT Gateway](https://docs.aws.amazon.com/vpc/latest/userguide/vpc-nat-gateway.html) |
| --------------- | --------------- | --------------- |
| Private | Infra Private Subnet | [GrayLog](https://www.graylog.org/) Server |
|  |  | Monitoring Server |
|  |  | Control Node Server | 
|  |  | [SonarQube](https://www.sonarsource.com/products/sonarqube/) Server | 
|  |  | [AWS OpenSearch](https://aws.amazon.com/opensearch-service/) Node | 
|  |  | [Consul](https://www.consul.io/) Server | 
|  | 2 ASG Private Subnets | [AWS ASG Instances](https://docs.aws.amazon.com/autoscaling/ec2/userguide/auto-scaling-groups.html) (Service) |
|  |  | 2 [AWS OpenSearch](https://aws.amazon.com/opensearch-service/) Nodes | 
|  |  | 2 [Consul](https://www.consul.io/) Servers | 

The scheme of interaction between instances and network resources is presented below:
![scheme](../pictures/vpc-scheme.png)

Private resources across domains are accessed using the AWS Enclave Web Server. AWS Enclave Web Server receives a domain certificate from AWS, receives ip-addresses and service ports from Consul. And with the help of Consul -Template creates a configuration file for Nginx with a reverse proxy to private instances.

## Consequences

This approach allows you to increase the security of the network infrastructure by closing private resources in private subnets.
