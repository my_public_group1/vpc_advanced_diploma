variable "bucket_name" {
  type = string
}

variable "dynamodb_name" {
  type = string
}

variable "aws_region" {
  type = string
}

variable "bucket_key_path_state" {
  type = string
  default = "adv_dip/terraform.tfstate"
}

variable "bucket_key_path_version" {
  type = string
  default = "kv-store/release_version"
}

variable "meta_name" {
  type = string
  default = "release-version"
}
