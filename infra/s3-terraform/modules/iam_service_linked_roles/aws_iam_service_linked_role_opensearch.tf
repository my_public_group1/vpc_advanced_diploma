resource "aws_iam_service_linked_role" "opensearch" {
  aws_service_name = "opensearchservice.amazonaws.com"
  provisioner "local-exec" {
    command = "sleep 10"
  }
}
