module "region" {
  source = "./modules/region"
}

module "aws-s3-state" {
  source          = "./modules/s3-state"
  bucket_name     = "${local.inputs.bucket_name}"
  dynamodb_name   = "${local.inputs.dynamodb_name}"
  aws_region      = "${module.region.aws_region}"
  # bucket_key_path_state = <your_path_to_state_object> #(Optional)
  # bucket_key_path_version =  <your_path_to_version_object> #(Optional)
  # meta_name = <name_of_metadata_key> #(Optional)
}

module "iam_service_linked_roles" {
  source          = "./modules/iam_service_linked_roles"
}
