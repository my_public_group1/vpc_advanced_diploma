# Instructions for working with SonarQube server.

Date: 2023-05-02

## Status

Accepted

## Context

We need to connect the [SonarQube](https://www.sonarsource.com/products/sonarqube/) server to [GitLab CI/CD](https://docs.gitlab.com/ee/ci/).

## Decision

##### After creating the infrastructure using Terraform, we need to link the SonarQube server and gitlab.com. To do this, perform the following steps:

1. First we need to create a Personal Access Token on gitlab.com. To do this, go to gitlab.com, log in to your account, click on your avatar on the top right and select "Preferences". Next, go to the "Access Token" tab and create a new token with any name and expiration date. This token must have read access to the API and repository. Let's keep it for ourselves:
![scheme](../pictures/gitlab_token.png)

2. First go to the SonarQube web interface for the following domains:

| Domain App (env) | Domain Test (env) |
| ----------- | ----------- |
| https://sonar.example.com/ | https://sonar-test.example.com/ |

3. On the page that opens, enter the login and password that was set in Terraform inputs;

![scheme](../pictures/sonar_login.png)

4. After login, on the project creation page, we need to select "From GitLab";

![scheme](../pictures/sonar_create.png)

5. In the window that opens, we need to enter the name of the configuration, the GitLab API URL, as well as the gitlab.com personal access token that was created in the first step;

![scheme](../pictures/sonar_config.png)

6. On the next page, enter the gitlab.com personal access token again and click "Save";

![scheme](../pictures/sonar_token.png)

7. On the page that appears, select the repository from our account that needs to be scanned;

![scheme](../pictures/sonar_select_repo.png)

8. Next, select the integration method "GitLab CI";

![scheme](../pictures/sonar_ci_select.png)

9. On the page that opens, in the first step, we need to select the programming language in which our project is written. And also, we need to copy the proposed content into the "sonar-project.properties" file and put it in the root of the repository with our project;

![scheme](../pictures/sonar_lang.png)
![scheme](../pictures/sonar_prop.png)

10. In the next step, we need to create two variables in GitLab CI. To do this, follow the instructions indicated in the screenshot;

![scheme](../pictures/sonar_vars.png)

11. We skip the next step. And at the last step, we understand that the setup is complete, and now SonarQube is waiting to be launched from GitLab CI;

![scheme](../pictures/sonar_skip.png)
![scheme](../pictures/sonar_wait.png)

12. After you push your code to the repository in the "main" branch or merge the "uat-*" branch into the "main" branch, the code is analyzed using SonarQube. We can see the results of the check on the same page of our SonarQube project, where there was a step with waiting for the launch; 

![scheme](../pictures/sonar_report.png)

## Consequences

Static code analysis during deployment is very useful for the security of our project, as it allows us to see bugs and vulnerabilities prematurely, as well as prevent the deployment of unsafe code to the production environment.
