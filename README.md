# Docker-GO-diploma

[![Build Status](https://gitlab.com/my_public_group1/basic_diploma/badges/main/pipeline.svg)](https://gitlab.com/my_public_group1/basic_diploma)

Данный проект является дипломной работой курса "DevOps-инженер. Advanced" и содержит в себе следующие этапы:

- Создание инфраструктуры в облаке Amazon AWS с помощью инструмента Terraform;
- Конфигурация инфраструктуры и deploy приложения с помощью Ansible и Docker;
- Тестирование приложения и запуск плейбука Ansible с помощью Gitlab CI/CD.

## Требования для запуска проекта:

1. Зарегистрируйте аккаунт [Amazon AWS](https://aws.amazon.com/account/sign-up);
2. Установите [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html) для управления аккаунтом и ресурсами;
3. Перейдите в `Identity and Access Management (IAM)` создайте пользователя `Administrator`, создайте группу `Administrators` с политикой доступа `AdministratorAccess` и добавьте только что созданного пользователя в эту группу;
4. Далее перейдите в вашего пользователя `Administrator`, выберите вкладку `Security Credentials`, создайте `Access Key`, и сохраните у себя `AWS Access Key ID` и `AWS Secret Access Key`;
5. [Cконфигурируйте AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html), используя раннее полученные значения `AWS Access Key ID` и `AWS Secret Access Key`;
6. Купите домен у любого регистратора или зарегистрировать бесплатный, [создать hosted zone в AWS Route 53](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/CreatingHostedZone.html) и [прописать NS-сервера у регистратора домена](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/domain-name-servers-glue-records.html);
7. Зарегистрируйте аккаунт [PagerDuty](https://www.pagerduty.com/sign-up/), создайте сервис получения уведомлений с интеграцией Prometheus, и сохраните у себя url и приватный ключ;
8. Установите [Terraform](https://www.terraform.io/downloads);
9. Установите [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html);
10. Сохраните себе _url_ репозитория _gitlab_ с проектом;
11. Выполните команду для генерации секретного ключа Graylog и сохраните его у себя:
- `pwgen -N 1 -s 96`
12. Оформите подписку на использование [AWS Certificate Manager for Nitro Enclaves](https://aws.amazon.com/marketplace/pp/prodview-f4gcl7narsmle), нажав на `Continue to Subscribe`.

## Описание инфраструктуры проекта:

Инфраструктура проекта предусматривает создание следующих элементов: 

| Элемент | Назначение |
| --- | ----------- |
| [AWS S3 Bucket](https://aws.amazon.com/s3/) | Корзина AWS S3 используется для хранения бэкапов состояния Terraform, хранения релизной версии проекта основной ветки `main`, а также для хранения логов [AWS Application Load Balancer](https://aws.amazon.com/elasticloadbalancing/application-load-balancer/) |
| [AWS DynamoDB](https://aws.amazon.com/dynamodb/) | NoSQL база данных, необходимая для хранения, записи, обработки состояний Terraform посредством корзины AWS S3 Bucket |
| [AWS ACM Certificates](https://aws.amazon.com/certificate-manager/) | Сертификаты необходимы для AWS ACM Enclave Web сервера и AWS Application Load Balancer, чтобы все публичные доступы по домену работали через протокол шифрования `HTTPS`. Также отдельный сертификат необходим для работы AWS Client VPN Endpoint |
| [AWS Virtual Private Cloud](https://aws.amazon.com/vpc/) | Необходимо для создания изолированной сетевой инфраструктуры |
| [AWS Subnets](https://docs.aws.amazon.com/vpc/latest/userguide/configure-subnets.html) | Необходимы для разделения сетевых ресурсов на публичные и приватные |
| [AWS Internet Gateway](https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Internet_Gateway.html) | Необходим для связывания ресурсов публичных подсетей с Интернетом и обратно |
| [AWS NAT Gateway](https://docs.aws.amazon.com/vpc/latest/userguide/vpc-nat-gateway.html) | Необходим для связывания ресурсов приватных подсетей с Интернетом. Он отправляет запросы от приватных подсетей как будто это ресурс из публичной подсети. |
| [AWS Route Tables](https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Route_Tables.html) | Необходимы для того, чтобы ресурсы публичных подсетей могли отправлять сетевые запросы на AWS Internet Gateway, а ресурсы приватных подсетей могли отправлять сетевые запросы на AWS NAT Gateway |
| [AWS ACL](https://docs.aws.amazon.com/AmazonS3/latest/userguide/acl-overview.html) | Необходимы для фильтрации траффика между подсетями |
| [AWS Client VPN Endpoint](https://docs.aws.amazon.com/vpn/latest/clientvpn-admin/cvpn-working-endpoints.html) | Необходим для подключения администратором к инстансам всех подсетей в целях конфигурации |
| [AWS OpenSearch](https://aws.amazon.com/opensearch-service/) | Аналитическая система, используемая для хранения и анализа различных журналов инфраструктуры |
| [AWS IAM](https://aws.amazon.com/iam/) | Создаются роли доступа к различным ресурсам, которую в свою очередь закрепляются за профилями доступа, которые закрепляются за инстансами, требующие эти права доступа |
| [Consul Servers](https://www.consul.io/) | Кластер из N-серверов, необходимый для работы Service Discovery, а именно для автоматического обнаружения серверов с зарегистрированными сервисами в кластере |
| [Graylog Server](https://www.graylog.org/) | Сервер для сбора логов, создания алертов, и записи их в AWS OpenSearch |
| [AWS Application Load Balancer](https://aws.amazon.com/elasticloadbalancing/application-load-balancer/) | Необходим для балансировки нагрузки между серверами приложения |
| [AWS Auto Scaling Group](https://aws.amazon.com/autoscaling/) | Группа автоматического масштабирования серверов приложения, которая в заваисимости от нагрузки увеличивает или уменьшает количество сервероа приложения | 
| Monitoring Server | Необходим для мониторинга всех серверов инфраструктуры |
| Control Node Server | Используется для работы gitlab-runner, а также для динамического создания inventory файла, и конфигурирования инстансов AWS Auto Scaling Group при деплое из gitlab |
| [SonarQube Server](https://www.sonarsource.com/products/sonarqube/) | Сервер, необходимый для статического анализа различного кода на ошибки и уязвимости |
| [AWS ACM Enclave Web Server](https://docs.aws.amazon.com/enclaves/latest/user/nitro-enclave-refapp.html) | Web-сервер на базе [Nginx](https://nginx.org/en/), [Consul-Template](https://developer.hashicorp.com/consul/tutorials/developer-configuration/consul-template) и `nitro-enclaves-acm`. Он работает как реверс-прокси сервер, распределяя доступ на инструменты, которые имеют веб-интерфейс, но находятся в приватной подсети. Web-сервер использует сертификат AWS ACM, который позволяет работать через протокол шифрования HTTPS. Также он использует Consul-Template для обнаруживания ip-адресов и портов необходимых инструментов, с которыми работает Nginx |
| [AWS Route53](https://aws.amazon.com/route53/) Records | DNS записи для приложения, инструментов мониторинга, Graylog и SonarQube серверов, а также для AWS OpenSearch Domain в системе Route53 |

> Таблица 1.

Общая схема взаимодействия между элементами инфраструктуры представлена ниже:

![scheme_process](pictures/aws-iac.png)

Схема, показывающая порядок создания элементов инфраструктуры, представлена ниже:

![scheme_create](pictures/aws_create_steps.png)

Инфраструктура создается следующим образом:

1. На первом шаге проиcходит запуск Terraform для создания AWS S3 Bucket, DynamoDB, объектов AWS S3 для хранения состояния Terraform и релизной версии проекта;

2. На втором шаге запускается Terraform для создания самой инфраструктуры. Он включает в себя следующие этапы:
- Создается сертификат для доменного имени, который потом будет применяться в AWS Application Load Balancer и AWS ACM Enclave Web Server для работы протокола шифрования HTTPS.
- Далее одновременно создаются роли и профили прав доступа к различным ресурсам, а также создается AWS VPC и все, что с ним связано (AWS Subnets, AWS Internet Gateway, AWS NAT Gateway, AWS Route Tables, AWS ACL).
- На следующем шаге одновременно запускаются два этапа: создание AWS Client VPN Endpoint и создание домена AWS OpenSearch Domain.
- После создания AWS Client VPN Endpoint запускается подготовка, создание и конфигуирование Consul серверов.
- Когда создание AWS OpenSearch Domain и Consul серверов завершится, произойдет создание GrayLog сервера. 
- После запуска GrayLog сервера одновременно выполняются следующие этапы: конфигурирование GrayLog сервера, запуск и конфигурирование Control-Node, Monitoring, SonarQube серверов, AWS Application Load Balancer и AWS Auto Scaling Group.
- После завершения предыдущего шага, происходит запуск и конфигурация AWS ACM Enclave Web Server.
- Далее создаются DNS записи в системе Route53 на AWS Application Load Balancer и AWS ACM Enclave Web Server. Они необходимы для доступа к различным инструментам по веб-интерфейсу, а также к самому приложению. 

Ниже представлены статьи, в которых подробно описаны некоторые системы и функции, внедренные в инфраструктуру:
- [Система мониторинга](https://gitlab.com/my_public_group1/vpc_advanced_diploma/-/blob/main/infra/monitoring-ladr.md)
- [AWS Client VPN Endpoint](https://gitlab.com/my_public_group1/vpc_advanced_diploma/-/blob/main/infra/vpn-ladr.md)
- [Использование релизной версии эталонного сервиса](https://gitlab.com/my_public_group1/vpc_advanced_diploma/-/blob/main/infra/release-version-ladr.md)
- [AWS VPC или сетевая инфраструктура](https://gitlab.com/my_public_group1/vpc_advanced_diploma/-/blob/main/infra/vpc-ladr.md)
- [AWS OpenSearch или система сбора, хранения и анализа журналов инфраструктуры](https://gitlab.com/my_public_group1/vpc_advanced_diploma/-/blob/main/infra/collect-logs-ladr.md)

## Как работать с gitlab:

Необходимо сохранить у себя токен для создания gitlab-runner, для этого выполните следующие действия:

1. Перейдите `Settings -> CI/CD -> Runners`;

2. В области `Shared Runners` отключите флажок `Enable shared runners for this project`;

3. В области `Specific Runners` найдите строку `And this registration token` и сохраните у себя токен, который следует за этой строкой. Этот токен необходим для создания новых gitlab-раннеров;

4.  Далее необходимо перейти в общие настройки gitlab. Для этого необходимо нажать на аватар профиля справа сверху и выбрать `Preferences`. На открывшейся странице необходимо выбрать вкладку `Access Token`. Далее создаем новый токен, выбираем для него любое имя и права доступа `api`, `read_api`. После создания появится токен `Your new personal access token`, его необходимо скопировать себе и сохранить. Этот токен необходим для запуска скрипта удаления не активных gitlab-раннеров.

## Как работать с проектом:
1. Склонируйте репозиторий к себе для работы с ним:

   ```shell
   git clone https://gitlab.com/my_public_group1/vpc_advanced_diploma.git
   ```
2. Далее необходимо сконфигурировать и запустить Terraform для создания корзины AWS S3 Bucket. Перейдите в директорию `infra/s3-terraform` и переименуйте файл `inputs.tf.example` в `inputs.tf`, после отредактриуйте следующие переменные:

| Переменная | Значение |
| --- | ----------- |
| bucket_name | Название корзины AWS S3 Bucket (должно быть уникально среди пользователей AWS, иначе будет ошибка) |
| dynamodb_name | Название базы данных DynamoDB (должно быть уникально среди пользователей AWS, иначе будет ошибка) |

> Таблица 2.

3. Инициализируйте Terraform и запустите создание ресурсов:

   ```shell
   terraform init
   terraform apply
   ```
4. Далее необходимо сконфигурировать и запустить Terraform для создания инфраструктуры проекта. Перейдите в директорию `infra/terraform` и переименуйте файл `inputs.tf.example` в `inputs.tf`, после отредактриуйте следующие переменные:

| Переменная | Значение |
| --- | ----------- |
| common_domain | Ваш домен, зона которого была сконфигрурирована в Route 53 |
| aws_access_key_id | ID ключа AWS CLI |
| aws_secret_access_key | Ключ AWS CLI |
| gitlab_repo | Ссылка на репозиторий с данным проектом |
| gitlab_token | Токен для создания gitlab-раннера |
| gitlab_access_api_token | Токен для доступа к API gitlab |
| os_domain | Имя домена AWS OpenSearch |
| opensearch_user | Имя пользователя для входа в OpenSearch |
| opensearch_password | Пароль для входа в OpenSearch |
| grafana_user | Имя пользователя для входа в grafana |
| grafana_pass | Пароль для входа в grafana |
| pagerduty_user_url | PagerDuty URL |
| pagerduty_user_key | PagerDuty Key |
| graylog_secret | Ключ Graylog (`pwgen -N 1 -s 96`) |
| graylog_password | Пароль для входа в GrayLog |
| sonar_password | Пароль для входа в SonarQube |
| psql_password | Пароль для входа в БД PostgreSQL от SonarQube |

> Таблица 3.

5. Инициализируйте Terraform с файлом конфигурации backend и создайте два рабочих пространства для тестового и продакшн окружений:

   ```shell
   terraform init -backend-config=backend.hcl
   terraform workspace new test
   terraform workspace new app
   ```

6. Выберите рабочие пространство для тестового окружения и запустите создание ресурсов AWS:

   ```shell
   terraform workspace select test
   terraform apply -var-file="s3.tfvars"
   ```

7. Далее необходимо выполнить аналогичные действия для создания продакшн окружения:

   ```shell
   terraform workspace select app
   terraform apply -var-file="s3.tfvars"
   ```

8. При создании инфраструктуры через Terraform, выполняется деплой приложения GO на инстансы AWS ASG с помощью локального запуска Ansible-Playbook посредством скрипта Bash. Приложение деплоится из ветки main, вне зависимости от того, в каком рабочем пространстве был запущен Terraform;

9. После создания ресурсов через `Terraform` создайте тестовую ветку с названием `uat-ddmmyy` выполните `commit` и `push` наших изменений на `gitlab.com` , что вызовет запуск `ansible playbook` для развертывания `docker` контейнеров с нашим `GO` приложением на тестовое окружение:

   ```shell
   git checkout -b uat-ddmmyy
   git add .
   git commit -m "init_test_commit"
   git push --set-upstream origin uat-221022
   ```

10. Далее, если в тестовом окружении все выполнилось без ошибок, и поднялось наше приложение, необходимо создать запрос на слияние (merge request) тестовой ветки и основной `main` на `gitlab.com`, где содержится код приложения. После подверждения данного запроса будет выполнен запуск ansible playbook для развертывания `docker` контейнеров с нашим `GO` приложением на продакшн окружение.

11. Результат выполнения можно увидеть после выполнения pipeline по следующим субдоменам `(доступен только https)`:

| Окружение | Субдомен |
| --- | ----------- |
| app | app.example.com |
| test | test.example.com |

> Таблица 4.

12. Инструменты мониторинга доступны по следующим доменам, в зависимости от окружения `(доступен только https)`:

| Окружение | Инструмент мониторинга | Субдомен |
| --- | ----------- | ----------- |
| app | Prometheus | prom.example.com |
|| Grafana | grafana.example.com |
|| Alertmanager | am.example.com |
|| VictoriaMetrics | victoria.example.com |
| test | Prometheus | prom-test.example.com |
|| Grafana | grafana-test.example.com |
|| Alertmanager | am-test.example.com |
|| VictoriaMetrics | victoria-test.example.com |

> Таблица 5.

13. Дэшборд AWS OpenSearch доступен по следующим доменам `(доступен только https)`:

| Окружение | Субдомен |
| --- | ----------- |
| app | os.example.com/_dashboards |
| test | os-test.example.com/_dashboards |

> Таблица 6.

14. Дэшборд GrayLog доступен по следующим доменам `(доступен только https)`:

| Окружение | Субдомен |
| --- | ----------- |
| app | graylog.example.com |
| test | graylog-test.example.com |

> Таблица 7.

15. Дэшборд SonarQube доступен по следующим доменам `(доступен только https)`:

| Окружение | Субдомен |
| --- | ----------- |
| app | sonar.example.com |
| test | sonar-test.example.com |

> Таблица 8.

## Как работать с SonarQube:

Данный проект содержит в себе SonarQube сервер, который может статически анализировать код проекта на ошибки и уязвимости. SonarQube сервер поднимается и доступен в обоих окружениях (app и test), но анализ кода может быть запущен только из ветки `main`. Поэтому использование SonarQube сервера в тестовом окружении не имеет смысла. Подробная инструкция по работе с SonarQube описаны в [этой статье](https://gitlab.com/my_public_group1/vpc_advanced_diploma/-/blob/main/service/sonarqube-man.md).
